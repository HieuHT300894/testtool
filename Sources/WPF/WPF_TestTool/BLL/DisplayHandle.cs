﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_TestTool.General;

namespace WPF_TestTool.BLL
{
    public static class DisplayHandle
    {
        public static string GetDisplay(this IWebDriver webDriver, IWebElement targetElement)
        {
            if (targetElement.TagName.ToEqualEx(Define.Instance.Input))
                return webDriver.GetValue(targetElement);
            else if (targetElement.TagName.ToEqualEx(Define.Instance.Button))
                return webDriver.GetValue(targetElement);
            else
                return webDriver.GetText(targetElement);
        }
        public static string GetValue(this IWebDriver webDriver, IWebElement targetElement)
        {
            //return targetElement.GetAttribute("value");

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"return arguments[0].value;");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            return js.ExecuteScript(sbScript.ToString(), targetElement) as string;
        }
        public static string GetText(this IWebDriver webDriver, IWebElement targetElement)
        {
            //return targetElement.Text;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"return arguments[0].textContent;");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            return js.ExecuteScript(sbScript.ToString(), targetElement) as string;
        }
    }
}
