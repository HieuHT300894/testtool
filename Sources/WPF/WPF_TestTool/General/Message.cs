﻿using System.Windows;
using System.Windows.Controls;

namespace WPF_TestTool.General
{
    public static class Message
    {
        public static void showMessage(this Window frmMain, string Title = "Message", string Caption = "") { MessageBox.Show(frmMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Information); }
        public static void showWarming(this Window frmMain, string Title = "Warming", string Caption = "") { MessageBox.Show(frmMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Warning); }
        public static void showError(this Window frmMain, string Title = "Error", string Caption = "") { MessageBox.Show(frmMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Error); }
        public static bool showConfirm(this Window frmMain, string Title = "Confirm", string Caption = "") { return MessageBox.Show(frmMain, Caption, Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes; }

        //public static void showMessage(this UserControl ucMain, string Title = "Message", string Caption = "") { MessageBox.Show(ucMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Information); }
        //public static void showWarming(this UserControl ucMain, string Title = "Warming", string Caption = "") { MessageBox.Show(ucMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Warning); }
        //public static void showError(this UserControl ucMain, string Title = "Error", string Caption = "") { MessageBox.Show(ucMain, Caption, Title, MessageBoxButton.OK, MessageBoxImage.Error); }
        //public static bool showConfirm(this UserControl ucMain, string Title = "Confirm", string Caption = "") { return MessageBox.Show(ucMain, Caption, Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes; }
    }
}
