﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup.Primitives;
using System.Windows.Media;
using System.Xml;

namespace WPF_TestTool.General
{
    public static class Extension
    {
        public static void InitDocument(this XmlDocument xmlDoc)
        {
            xmlDoc.LoadXml($"<{Define.Instance.Page}></{Define.Instance.Page}>");

            XmlDeclaration xmlDec = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.InsertBefore(xmlDec, xmlDoc.DocumentElement);
        }
        public static XmlNode AppendChildEx(this XmlDocument xmlDoc, XmlNode parentNode, string childName, Dictionary<string, string> attrs)
        {
            XmlNode childNode = xmlDoc.CreateElement(childName);
            attrs.ToList().ForEach(x =>
            {
                XmlAttribute xmlAttr = xmlDoc.CreateAttribute(x.Key);
                xmlAttr.Value = x.Value;
                childNode.Attributes.Append(xmlAttr);
            });
            parentNode.AppendChild(childNode);

            return childNode;
        }

        public static bool WaitForLoad(this IWebDriver driver)
        {
            try
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                if (!js.ExecuteScript("return document.readyState").Equals("complete"))
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool GetElement(this IWebDriver driver, string sTarget, bool bPosition, int iX, int iY)
        {
            try
            {
                StringBuilder sbScript = new StringBuilder();
                sbScript.AppendLine($"var obj = {{}};");
                sbScript.AppendLine($"obj.width = 10;");
                sbScript.AppendLine($"obj.height = 20;");
                sbScript.AppendLine($"return obj;");

                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                object obj = js.ExecuteScript(sbScript.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void Log(this Exception ex)
        {
            try
            {
                if (!Directory.Exists(Define.Instance.dir))
                    Directory.CreateDirectory(Define.Instance.dir);

                File.AppendAllText($@"{Define.Instance.dir}\{Define.Instance.errorFile}", $"{DateTime.Now.ToString()} {ex.Message}{Environment.NewLine}");
            }
            catch
            {

            }
        }
        public static void Log(this KeyValuePair<string, object> kv)
        {
            try
            {
                if (!Directory.Exists(Define.Instance.dir))
                    Directory.CreateDirectory(Define.Instance.dir);

                File.AppendAllText($@"{Define.Instance.dir}\{Define.Instance.logFile}", $"{DateTime.Now.ToString()} [{kv.Key}]=[{kv.Value}]{Environment.NewLine}");
            }
            catch
            {

            }
        }

        public static bool ToEqualEx(this string text1, string text2)
        {
            if (text1 == null || text2 == null)
                return false;

            text1 = text1.Trim();
            text2 = text2.Trim();
            return text1.ToLowerEx().Equals(text2.ToLowerEx());
        }
        public static bool ToContainEx(this string text1, string text2)
        {
            if (text1 == null || text2 == null)
                return false;

            text1 = text1.Trim();
            text2 = text2.Trim();
            return text1.ToLowerEx().Contains(text2.ToLowerEx());
        }
        public static bool ToStartsWithEx(this string text1, string text2)
        {
            if (text1 == null || text2 == null)
                return false;

            text1 = text1.Trim();
            text2 = text2.Trim();
            return text1.ToLowerEx().StartsWith(text2.ToLowerEx());
        }
        public static string ToLowerEx(this string text1)
        {
            if (text1 == null)
                return text1;

            return text1.ToLower(CultureInfo.CurrentCulture);
        }
        public static bool IsEmpty(this string text)
        {
            return string.IsNullOrWhiteSpace(text.Trim());
        }
        public static bool IsNotEmpty(this string text)
        {
            return !string.IsNullOrWhiteSpace(text.Trim());
        }

        public static void Format(this GridSplitter gsMain)
        {
            gsMain.HorizontalAlignment = HorizontalAlignment.Stretch;

            if (gsMain.Style != null)
                return;

            Setter setterMouseOver = new Setter();
            setterMouseOver.Property = Control.BackgroundProperty;
            setterMouseOver.Value = Brushes.Gray;

            Trigger trigger = new Trigger();
            trigger.Property = UIElement.IsMouseOverProperty;
            trigger.Value = true;
            trigger.Setters.Add(setterMouseOver);

            Setter setter = new Setter();
            setter.Property = Control.BackgroundProperty;
            setter.Value = Brushes.Transparent;

            Style style = new Style();
            style.TargetType = typeof(GridSplitter);
            style.Setters.Add(setter);
            style.Triggers.Add(trigger);

            gsMain.Style = style;
        }
        public static void Format(this Slider sliderMain, double Minimum = 0, double Maximum = 100, double SmallChange = 1)
        {
            sliderMain.Minimum = Minimum;
            sliderMain.Maximum = Maximum;
            sliderMain.SmallChange = SmallChange;
        }
        public static void Format(this FrameworkElement fElement, Thickness thickness, double Width = 100, double Height = 25)
        {
            fElement.Margin = thickness;
            fElement.Width = Width;
            fElement.Height = Height;
        }

        //public static void Format(this Button btnMain, Thickness thickness, double Width = 100, double Height = 25)
        //{
        //}
        //public static void Format(this ComboBox cbbMain, Thickness thickness, double Width = 100, double Height = 25)
        //{
        //}
        //public static void Format(this ListBox lbMain, Thickness thickness, double Width = 100, double Height = 25)
        //{
        //}
        //public static void Format(this DockPanel pnMain, Thickness thickness, double Width = 100, double Height = 25)
        //{
        //}

        //public static void Format(this NumericUpDown number)
        //{
        //    number.Minimum = 0;
        //    number.Maximum = int.MaxValue;
        //    number.TextAlign = HorizontalAlignment.Center;
        //}
        //public static void Format(this TextBox txt)
        //{
        //    txt.Multiline = true;
        //    txt.Height = 50;
        //}
        //public static void FormatEx(this ComboBox cb, string ValueMember, string DisplayMember)
        //{
        //    cb.DropDownStyle = ComboBoxStyle.DropDownList;
        //    cb.ValueMember = ValueMember;
        //    cb.DisplayMember = DisplayMember;
        //}
        //public static void FormatEx(this ListBox lb, string ValueMember, string DisplayMember)
        //{
        //    lb.ValueMember = ValueMember;
        //    lb.DisplayMember = DisplayMember;
        //}
    }
}
