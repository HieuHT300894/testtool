﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace WPF_TestTool.Model
{
    public class Step
    {
        //public ChromiumWebBrowser Browser { get; set; }
        //public List< Action<ChromiumWebBrowser>> Actions { get; set; }

        public string Name { get; set; }
        public bool Status { get; set; }
        public Func<IWebDriver, string> Func { get; set; }
    }
}
