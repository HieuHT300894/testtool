﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_TestTool.General;

namespace WPF_TestTool.Model
{
    public class XmlParser : TextParser
    {
        public XmlParser()
        {
        }

        public XmlParser(string xml) : base(xml)
        {
        }

        /// <summary>
        /// Parses the next tag that matches the specified tag name
        /// </summary>
        /// <param name="name">Name of the tags to parse ("*" = parse all tags)</param>
        /// <param name="tagIn">Returns information on the next occurrence of the
        /// specified tag or null if none found</param>
        /// <returns>True if a tag was parsed or false if the end of the document was reached</returns>
        public bool ParseNext(string name, out Tag tagIn)
        {
            // Must always set out parameter
            tagIn = null;

            // Nothing to do if no tag specified
            if (string.IsNullOrEmpty(name))
                return false;

            // Loop until match is found or no more tags
            MoveTo('<');
            while (!EndOfText)
            {
                // Skip over opening '<'
                MoveAhead();

                // Examine first tag character
                char c = Peek();
                if (c == '!' && Peek(1) == '-' && Peek(2) == '-')
                {
                    // Skip over comments
                    const string endComment = "-->";
                    MoveTo(endComment);
                    MoveAhead(endComment.Length);
                }
                else if (c == '/')
                {
                    // Skip over closing tags
                    MoveTo('>');
                    MoveAhead();
                }
                else
                {
                    // Return true if requested tag was found
                    if (ParseTag(name, ref tagIn))
                        return true;
                }
                // Find next tag
                MoveTo('<');
            }
            // No more matching tags found
            return false;
        }

        /// <summary>
        /// Parses the contents of an HTML tag. The current position should be at the first
        /// character following the tag's opening less-than character.
        /// 
        /// Note: We parse to the end of the tag even if this tag was not requested by the
        /// caller. This ensures subsequent parsing takes place after this tag
        /// </summary>
        /// <param name="reqName">Name of the tag the caller is requesting, or "*" if caller
        /// is requesting all tags</param>
        /// <param name="tag">Returns information on this tag if it's one the caller is
        /// requesting</param>
        /// <param name="inScript">Returns true if tag began, and did not end, and script
        /// block</param>
        /// <returns>True if data is being returned for a tag requested by the caller
        /// or false otherwise</returns>
        protected bool ParseTag(string reqName, ref Tag tag)
        {
            bool requested = false;
            int indicate = 0;

            // Get name of this tag
            string name = ParseTagName(ref indicate);

            // Special handling
            if (string.Compare(name, "?xml", true) == 0)
                return false;

            // Is this a tag requested by caller?
            if (reqName == "*" || string.Compare(name, reqName, true) == 0)
            {
                // Yes
                requested = true;
                // Create new tag object
                tag = new Tag();
                tag.Name = name;
                tag.StartOffset = indicate - 1;
                tag.Attributes = new List<Attribute>();
            }

            // Parse attributes
            MovePastWhitespace();
            while (Peek() != '>' && Peek() != NullChar)
            {
                if (Peek() == '/')
                {
                    MoveAhead();
                    MovePastWhitespace();
                }
                else
                {
                    int nameIndicate = 0;
                    int valueIndicate = 0;

                    // Parse attribute name
                    name = ParseAttributeName(ref nameIndicate);
                    MovePastWhitespace();
                    // Parse attribute value
                    string value = string.Empty;
                    if (Peek() == '=')
                    {
                        MoveAhead();
                        MovePastWhitespace();
                        value = ParseAttributeValue(ref valueIndicate);
                        MovePastWhitespace();
                    }
                    // Add attribute to collection if requested tag
                    if (requested && name.IsNotEmpty())
                    {
                        // This tag replaces existing tags with same name
                        Attribute attr = tag.GetAttribute(name);
                        if (attr == null)
                            tag.Attributes.Add(attr = new Attribute());
                        attr.Name = name;
                        attr.Value = value;
                        attr.NameStartOffset = nameIndicate;
                        attr.ValueStartOffset = valueIndicate;
                    }
                }
            }
            // Skip over closing '>'
            MoveAhead();

            return requested;
        }

        /// <summary>
        /// Parses a tag name. The current position should be the first character of the name
        /// </summary>
        /// <returns>Returns the parsed name string</returns>
        protected string ParseTagName(ref int indicate)
        {
            indicate = Position;
            while (!EndOfText && !char.IsWhiteSpace(Peek()) && Peek() != '>')
                MoveAhead();
            return Substring(indicate, Position);
        }

        /// <summary>
        /// Parses an attribute name. The current position should be the first character
        /// of the name
        /// </summary>
        /// <returns>Returns the parsed name string</returns>
        protected string ParseAttributeName(ref int indicate)
        {
            indicate = Position;
            while (!EndOfText && !char.IsWhiteSpace(Peek()) && Peek() != '>' && Peek() != '=')
                MoveAhead();
            return Substring(indicate, Position);
        }

        /// <summary>
        /// Parses an attribute value. The current position should be the first non-whitespace
        /// character following the equal sign.
        /// 
        /// Note: We terminate the name or value if we encounter a new line. This seems to
        /// be the best way of handling errors such as values missing closing quotes, etc.
        /// </summary>
        /// <returns>Returns the parsed value string</returns>
        protected string ParseAttributeValue(ref int indicate)
        {
            int end;
            char c = Peek();
            if (c == '"' || c == '\'')
            {
                // Move past opening quote
                MoveAhead();
                // Parse quoted value
                indicate = Position;
                MoveTo(new char[] { c, '\r', '\n' });
                end = Position;
                // Move past closing quote
                if (Peek() == c)
                    MoveAhead();
            }
            else
            {
                // Parse unquoted value
                indicate = Position;
                while (!EndOfText && !Char.IsWhiteSpace(c) && c != '>')
                {
                    MoveAhead();
                    c = Peek();
                }
                end = Position;
            }
            return Substring(indicate, end);
        }
    }
}
