﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_TestTool.Model
{
    public class Attribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int NameStartOffset { get; set; }
        public int NameEndOffset { get; set; }
        public int ValueStartOffset { get; set; }
        public int ValueEndOffset { get; set; }
    }
}
