﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_TestTool.General;

namespace WPF_TestTool.Model
{
    public class Tag
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StartLine { get; set; }
        public int EndLine { get; set; }
        public int StartOffset { get; set; }
        public int EndOffset { get; set; }
        public int OpenOffset { get; set; }
        public int OpenStartOffset { get; set; }
        public int OpenEndOffset { get; set; }
        public int CloseOffset { get; set; }
        public int CloseStartOffset { get; set; }
        public int CloseEndOffset { get; set; }
        public bool IsFolding { get; set; }
        /// <summary>
        /// true: have tagName close; false: don't have tagName close
        /// </summary>
        public bool IsType { get; set; }
        public int Level { get; set; }
        public int Length { get; set; }

        public List<Attribute> Attributes { get; set; }
        public bool HasAttribute(string name)
        {
            return Attributes.Any(x => x.Name.ToEqualEx(name));
        }
        public Attribute GetAttribute(string name)
        {
            return Attributes.FirstOrDefault(x => x.Name.ToEqualEx(name));
        }
    }
}
