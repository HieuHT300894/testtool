﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_TestTool.General;

namespace WPF_TestTool.Model
{
    public class KeyData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        /// <summary>
        /// 1: Modifier; 2: Other
        /// </summary>
        public int Type { get; set; }

        static List<KeyData> lstAllKeys;
        static List<KeyData> lstModifierKeys;
        static List<KeyData> lstOtherKeys;

        public static List<KeyData> GetAllKeys()
        {
            if (lstAllKeys == null)
            {
                lstAllKeys = new List<KeyData>();
                lstAllKeys.AddRange(GetModifierKeys());
                lstAllKeys.AddRange(GetOtherKeys());
            }
            return lstAllKeys;
        }
        public static List<KeyData> GetModifierKeys()
        {
            if (lstModifierKeys == null)
            {
                lstModifierKeys = new List<KeyData>();
                List<string> lstKeys = typeof(OpenQA.Selenium.Keys).GetFields().Select(x => x.Name).OrderBy(x => x).ToList();
                lstKeys.ForEach(x =>
                {
                    if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                });
            }
            return lstModifierKeys;
        }
        public static List<KeyData> GetOtherKeys()
        {
            if (lstOtherKeys == null)
            {
                lstOtherKeys = new List<KeyData>();
                List<string> lstKeys = typeof(OpenQA.Selenium.Keys).GetFields().Select(x => x.Name).OrderBy(x => x).ToList();
                lstKeys.ForEach(x =>
                {
                    if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) { }
                    else { lstOtherKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 2 }); }
                });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.A.ToString(), Name = System.Windows.Forms.Keys.A.ToString(), Value = System.Windows.Forms.Keys.A.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.B.ToString(), Name = System.Windows.Forms.Keys.B.ToString(), Value = System.Windows.Forms.Keys.B.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.C.ToString(), Name = System.Windows.Forms.Keys.C.ToString(), Value = System.Windows.Forms.Keys.C.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.D.ToString(), Name = System.Windows.Forms.Keys.D.ToString(), Value = System.Windows.Forms.Keys.D.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.E.ToString(), Name = System.Windows.Forms.Keys.E.ToString(), Value = System.Windows.Forms.Keys.E.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.F.ToString(), Name = System.Windows.Forms.Keys.F.ToString(), Value = System.Windows.Forms.Keys.F.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.G.ToString(), Name = System.Windows.Forms.Keys.G.ToString(), Value = System.Windows.Forms.Keys.G.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.H.ToString(), Name = System.Windows.Forms.Keys.H.ToString(), Value = System.Windows.Forms.Keys.H.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.I.ToString(), Name = System.Windows.Forms.Keys.I.ToString(), Value = System.Windows.Forms.Keys.I.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.J.ToString(), Name = System.Windows.Forms.Keys.J.ToString(), Value = System.Windows.Forms.Keys.J.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.K.ToString(), Name = System.Windows.Forms.Keys.K.ToString(), Value = System.Windows.Forms.Keys.K.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.L.ToString(), Name = System.Windows.Forms.Keys.L.ToString(), Value = System.Windows.Forms.Keys.L.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.M.ToString(), Name = System.Windows.Forms.Keys.M.ToString(), Value = System.Windows.Forms.Keys.M.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.N.ToString(), Name = System.Windows.Forms.Keys.N.ToString(), Value = System.Windows.Forms.Keys.N.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.O.ToString(), Name = System.Windows.Forms.Keys.O.ToString(), Value = System.Windows.Forms.Keys.O.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.P.ToString(), Name = System.Windows.Forms.Keys.P.ToString(), Value = System.Windows.Forms.Keys.P.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Q.ToString(), Name = System.Windows.Forms.Keys.Q.ToString(), Value = System.Windows.Forms.Keys.Q.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.R.ToString(), Name = System.Windows.Forms.Keys.R.ToString(), Value = System.Windows.Forms.Keys.R.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.S.ToString(), Name = System.Windows.Forms.Keys.S.ToString(), Value = System.Windows.Forms.Keys.S.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.T.ToString(), Name = System.Windows.Forms.Keys.T.ToString(), Value = System.Windows.Forms.Keys.T.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.U.ToString(), Name = System.Windows.Forms.Keys.U.ToString(), Value = System.Windows.Forms.Keys.U.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.V.ToString(), Name = System.Windows.Forms.Keys.V.ToString(), Value = System.Windows.Forms.Keys.V.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.W.ToString(), Name = System.Windows.Forms.Keys.W.ToString(), Value = System.Windows.Forms.Keys.W.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.X.ToString(), Name = System.Windows.Forms.Keys.X.ToString(), Value = System.Windows.Forms.Keys.X.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Y.ToString(), Name = System.Windows.Forms.Keys.Y.ToString(), Value = System.Windows.Forms.Keys.Y.ToString().ToLower(), Type = 2 });
                //lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Z.ToString(), Name = System.Windows.Forms.Keys.Z.ToString(), Value = System.Windows.Forms.Keys.Z.ToString().ToLower(), Type = 2 });
            }
            return lstOtherKeys;
        }
    }
}
