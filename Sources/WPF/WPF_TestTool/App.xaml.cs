﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF_TestTool.General;

namespace WPF_TestTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                StartupUri = new Uri($"GUI/frmMain.xaml", UriKind.RelativeOrAbsolute);
                base.OnStartup(e);
            }
            catch (Exception ex)
            {
                ex.Log();
            }
        }
    }
}
