﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using WPF_TestTool.General;

namespace WPF_TestTool.GUI
{
    public class frmBase : Window
    {
        public frmBase()
        {
            Loaded -= FrmBase_Loaded;
            SizeChanged -= FrmBase_SizeChanged;
            Closing -= FrmBase_Closing;

            Loaded += FrmBase_Loaded;
            SizeChanged += FrmBase_SizeChanged;
            Closing += FrmBase_Closing;
        }

        protected virtual void FrmBase_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }
        protected virtual void FrmBase_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
        protected virtual void FrmBase_Loaded(object sender, RoutedEventArgs e)
        {
        }

        public virtual void FormatForm()
        {
            Width = 900;
            Height = 450;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
        public virtual void FormatControl()
        {

        }
    }
}
