﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF_TestTool.General;
using WPF_TestTool.Model;
using WPF_TestTool.UC;
using System.ComponentModel;
using OpenQA.Selenium.Chrome;

namespace WPF_TestTool.GUI
{
    /// <summary>
    /// Interaction logic for frmMain.xaml
    /// </summary>
    public partial class frmMain : frmBase
    {
        public delegate void SendStatus(int index, string msg);
        public SendStatus sendStatus;

        IWebDriver driverChrome;
        int iStatus = Define.Instance.Stop;
        Action closeWindow = null;

        public frmMain()
        {
            InitializeComponent();
        }

        protected override void FrmBase_Loaded(object sender, RoutedEventArgs e)
        {
            ShowEditor();
            DisableEvents();
            EnableEvents();

            FormatForm();
            FormatControl();
        }
        protected override void FrmBase_Closing(object sender, CancelEventArgs e)
        {
            closeWindow = closeWindow ?? new Action(() =>
            {
                Closing -= FrmBase_Closing;

                Shutdown();
                e.Cancel = false;
            });

            if (iStatus == Define.Instance.Start)
                Stop();

            if (iStatus == Define.Instance.Stop)
                closeWindow.Invoke();
            else
                e.Cancel = true;
        }

        void Setup()
        {
            try
            {
                if (driverChrome == null)
                {
                    driverChrome = new ChromeDriver();
                    driverChrome.Manage().Window.Maximize();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
                this.showError(Caption: ex.Message);
            }
        }
        void Shutdown()
        {
            try
            {
                if (driverChrome != null)
                {
                    driverChrome.Quit();
                }
            }
            catch (Exception ex)
            {
                ex.Log();
                this.showError(Caption: ex.Message);
            }

        }
        async void Start(int startIndex = -1, List<Step> lstSteps = null)
        {
            //BeginInvoke(new Action(() => { lbLogs.Items.Clear(); }));

            if (startIndex == -1 || lstSteps == null || !lstSteps.Any())
                return;

            Setup();
            iStatus = Define.Instance.Start;
            sendStatus?.Invoke(iStatus, string.Empty);
            //BeginInvoke(new Action(() => { lbLogs.Items.Add($"Started"); }));

            int i = 0;
            int len = lstSteps.Count;

            while (i < len)
            {
                //Get step
                Step step = lstSteps[i];

                //Goto next step
                sendStatus?.Invoke(startIndex, string.Empty);

                //Wait running
                await Task.Delay(Define.Instance.DelayValue);

                //Close after click stop or close window
                if (iStatus == Define.Instance.Abort)
                {
                    closeWindow?.Invoke();
                    break;
                }

                //Run step
                string result = await Task.Factory.StartNew(() =>
                {
                    return step.Func(driverChrome);
                });

                //Set log status
                if (!string.IsNullOrWhiteSpace(result))
                {
                    //BeginInvoke(new Action(() => { lbLogs.Items.Add($"{step.Name} - {result}"); }));
                }
                else
                {
                    //BeginInvoke(new Action(() => { lbLogs.Items.Add($"{step.Name} - {Define.Instance.OK}"); }));
                }

                //Send result after running
                sendStatus?.Invoke(startIndex, result);

                i++;
                startIndex++;
            }

            iStatus = Define.Instance.Stop;
            sendStatus?.Invoke(iStatus, string.Empty);
            //BeginInvoke(new Action(() => { lbLogs.Items.Add($"Finished"); }));
        }
        void Stop()
        {
            iStatus = Define.Instance.Abort;
        }
        void ShowEditor()
        {
            if (pnControl.Children.Count == 0)
            {
                UC_Editor uc = new UC_Editor(this);
                uc.Name = nameof(UC_Editor);
                uc.startRun = Start;
                uc.stopRun = Stop;
                pnControl.Children.Add(uc);
            }
        }
        void DisableEvents()
        {

        }
        void EnableEvents()
        {
        }
        public override void FormatForm()
        {
            base.FormatForm();

            MinWidth = 900;
            MinHeight = 450;
        }
        public override void FormatControl()
        {
            lbLogs.Format(new Thickness(Define.Instance.Margin), double.NaN, double.NaN);
            gsMain.Format(new Thickness(Define.Instance.Margin), 3, double.NaN);
            pnControl.Format(new Thickness(Define.Instance.Margin), double.NaN, double.NaN);

            gsMain.Format();
        }
    }
}
