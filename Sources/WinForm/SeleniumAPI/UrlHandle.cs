﻿using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class UrlHandle
    {
        public static Func<string> GetFunc(UrlObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        try
                        {
                            ChromeHandle.GoToUrl(obj.sUrl);
                            ChromeHandle.Screenshot();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class UrlObject
    {
        public string sUrl { get; set; } = string.Empty;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
