﻿var _window = {
    OuterLocationX: window.screenX,
    OuterLocationY: window.screenY,
    OuterSizeWidth: window.outerWidth,
    OuterSizeHeight: window.outerHeight,
    InnerLocationX: window.screenX,
    InnerLocationY: window.screenY + window.outerHeight - window.innerHeight,
    InnerSizeWidth: window.innerWidth,
    InnerSizeHeight: window.innerHeight
};