﻿using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class PopupHandle
    {
        public static Func<string> GetFunc(PopupObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        try
                        {
                            if (obj.iType == 1)
                            {
                                int length = ChromeHandle.WindowHandles.Count;
                                if (length < 2)
                                    return false;

                                string lastHandle = ChromeHandle.WindowHandles[length - 1];
                                string lastLastHandle = ChromeHandle.WindowHandles[length - 2];

                                wd.SwitchTo().Window(lastHandle).Close();
                                wd.SwitchTo().Window(lastLastHandle);

                                ChromeHandle.CurrentWindowHandle = lastLastHandle;
                                ChromeHandle.WindowHandles.RemoveAt(length - 1);
                            }
                            else if (obj.iType == 2)
                            {
                                List<string> handles = wd.WindowHandles.ToList();
                                string lastHandle = handles.Last();

                                wd.SwitchTo().Window(lastHandle);

                                ChromeHandle.CurrentWindowHandle = lastHandle;
                                ChromeHandle.WindowHandles.Add(lastHandle);
                            }
                            else if (obj.iType == 3)
                            {

                            }
                            else if (obj.iType == 4)
                            {

                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class PopupObject
    {
        public int iType { get; set; } = 0;
        public int iWidth { get; set; } = 0;
        public int iHeight { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
