﻿using Common;
using OpenQA.Selenium;
using System.Drawing;
using System.Text;

namespace SeleniumAPI
{
    public static class XPathGenerate
    {
        public static string GetXpath(Point location)
        {
            if (ChromeHandle.ChromeDriver == null || ChromeHandle.ElementToXPathScript.IsEmpty())
                return string.Empty;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"{ChromeHandle.ElementToXPathScript}");
            sbScript.AppendLine($"{ChromeHandle.WindowRectScript}");
            sbScript.AppendLine($"var _element = document.elementFromPoint({location.X} - _window.InnerLocationX, {location.Y} - _window.InnerLocationY);");
            sbScript.AppendLine($"console.log(_element);");
            sbScript.AppendLine($"return getElementXpath(_element);");
            IJavaScriptExecutor js = ChromeHandle.ChromeDriver as IJavaScriptExecutor;
            return (js.ExecuteScript(sbScript.ToString()) as string) ?? string.Empty;
        }
        public static string GetXpath(int x, int y)
        {
            if (ChromeHandle.ChromeDriver == null || ChromeHandle.ElementToXPathScript.IsEmpty())
                return string.Empty;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"{ChromeHandle.ElementToXPathScript}");
            sbScript.AppendLine($"{ChromeHandle.WindowRectScript}");
            sbScript.AppendLine($"var _element = document.elementFromPoint({x} - _window.InnerLocationX, {y} - _window.InnerLocationY);");
            sbScript.AppendLine($"console.log(_element);");
            sbScript.AppendLine($"return getElementXpath(_element);");
            IJavaScriptExecutor js = ChromeHandle.ChromeDriver as IJavaScriptExecutor;
            return (js.ExecuteScript(sbScript.ToString()) as string) ?? string.Empty;
        }
    }
}
