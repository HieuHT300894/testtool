﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class PropertiesHandle
    {
        public static Func<string> GetFunc(PropertiesObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        Actions builder = new Actions(wd);
                        IWebElement element = null;
                        try
                        {
                            element = wd.FindElement(By.XPath(obj.sTarget));
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }

                        try
                        {
                            DOMRect rect = element.GetElementRect();
                            Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                            if (obj.bDisplayed && element.Displayed != obj.bDisplayed_Checked)
                                return false;
                            if (obj.bEnabled && element.Enabled != obj.bEnabled_Checked)
                                return false;
                            if (obj.bSelected && element.Selected != obj.bSelected_Checked)
                                return false;
                            if (obj.bLocation && (element.Location.X != obj.iLocation_X || element.Location.Y != obj.iLocation_Y))
                                return false;
                            if (obj.bSize && (element.Size.Width != obj.iSize_Width || element.Size.Height != obj.iSize_Height))
                                return false;
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class PropertiesObject
    {
        public bool bDisplayed { get; set; } = false;
        public bool bEnabled { get; set; } = false;
        public bool bSelected { get; set; } = false;
        public bool bLocation { get; set; } = false;
        public bool bSize { get; set; } = false;
        public bool bDisplayed_Checked { get; set; } = false;
        public bool bEnabled_Checked { get; set; } = false;
        public bool bSelected_Checked { get; set; } = false;
        public int iLocation_X { get; set; } = 0;
        public int iLocation_Y { get; set; } = 0;
        public int iSize_Width { get; set; } = 0;
        public int iSize_Height { get; set; } = 0;
        public string sTarget { get; set; } = string.Empty;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
