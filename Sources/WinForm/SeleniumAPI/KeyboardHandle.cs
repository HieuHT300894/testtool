﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SeleniumAPI
{
    public static class KeyboardHandle
    {
        public static Func<string> GetFunc(KeyboardObject obj)
        {
            List<string> modifierKeys = new List<string>();
            List<string> otherKeys = new List<string>();

            if (obj.bPosition)
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                if (element != null)
                                {
                                    DOMRect rect = element.GetElementRect();
                                    Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                    if (obj.sKey.IsNotEmpty())
                                    {
                                        if (obj.iKeyType == 0)
                                            SetKeyPress(builder.MoveToElement(element, element.Location.X + obj.iX, element.Location.Y + obj.iY), obj.sKey);
                                        else if (obj.iKeyType == 1)
                                            SetKeyDown(builder.MoveToElement(element, element.Location.X + obj.iX, element.Location.Y + obj.iY), obj.sKey);
                                        else if (obj.iKeyType == 2)
                                            SetKeyUp(builder.MoveToElement(element, element.Location.X + obj.iX, element.Location.Y + obj.iY), obj.sKey);
                                    }
                                }
                                else
                                {
                                    if (obj.sKey.IsNotEmpty())
                                    {
                                        if (obj.iKeyType == 0)
                                            SetKeyPress(builder.MoveByOffset(obj.iX, obj.iY), obj.sKey);
                                        else if (obj.iKeyType == 1)
                                            SetKeyDown(builder.MoveByOffset(obj.iX, obj.iY), obj.sKey);
                                        else if (obj.iKeyType == 2)
                                            SetKeyUp(builder.MoveByOffset(obj.iX, obj.iY), obj.sKey);
                                    }
                                }


                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
            else
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                DOMRect rect = element.GetElementRect();
                                Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                if (obj.sKey.IsNotEmpty())
                                {
                                    if (obj.iKeyType == 0)
                                        SetKeyPress(builder.MoveToElement(element), obj.sKey);
                                    else if (obj.iKeyType == 1)
                                        SetKeyDown(builder.MoveToElement(element), obj.sKey);
                                    else if (obj.iKeyType == 2)
                                        SetKeyUp(builder.MoveToElement(element), obj.sKey);
                                }
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
        }
        static void SetKeyPress(Actions builder, string key)
        {
            if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Add))) builder.SendKeys(OpenQA.Selenium.Keys.Add);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.ArrowDown))) builder.SendKeys(OpenQA.Selenium.Keys.ArrowDown);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.ArrowLeft))) builder.SendKeys(OpenQA.Selenium.Keys.ArrowLeft);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.ArrowRight))) builder.SendKeys(OpenQA.Selenium.Keys.ArrowRight);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.ArrowUp))) builder.SendKeys(OpenQA.Selenium.Keys.ArrowUp);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Backspace))) builder.SendKeys(OpenQA.Selenium.Keys.Backspace);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Cancel))) builder.SendKeys(OpenQA.Selenium.Keys.Cancel);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Clear))) builder.SendKeys(OpenQA.Selenium.Keys.Clear);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Decimal))) builder.SendKeys(OpenQA.Selenium.Keys.Decimal);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Delete))) builder.SendKeys(OpenQA.Selenium.Keys.Delete);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Divide))) builder.SendKeys(OpenQA.Selenium.Keys.Divide);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Down))) builder.SendKeys(OpenQA.Selenium.Keys.Down);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.End))) builder.SendKeys(OpenQA.Selenium.Keys.End);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Enter))) builder.SendKeys(OpenQA.Selenium.Keys.Enter);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Equal))) builder.SendKeys(OpenQA.Selenium.Keys.Equal);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Escape))) builder.SendKeys(OpenQA.Selenium.Keys.Escape);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F1))) builder.SendKeys(OpenQA.Selenium.Keys.F1);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F10))) builder.SendKeys(OpenQA.Selenium.Keys.F10);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F11))) builder.SendKeys(OpenQA.Selenium.Keys.F11);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F12))) builder.SendKeys(OpenQA.Selenium.Keys.F12);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F2))) builder.SendKeys(OpenQA.Selenium.Keys.F2);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F3))) builder.SendKeys(OpenQA.Selenium.Keys.F3);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F4))) builder.SendKeys(OpenQA.Selenium.Keys.F4);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F5))) builder.SendKeys(OpenQA.Selenium.Keys.F5);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F6))) builder.SendKeys(OpenQA.Selenium.Keys.F6);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F7))) builder.SendKeys(OpenQA.Selenium.Keys.F7);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F8))) builder.SendKeys(OpenQA.Selenium.Keys.F8);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.F9))) builder.SendKeys(OpenQA.Selenium.Keys.F9);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Help))) builder.SendKeys(OpenQA.Selenium.Keys.Help);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Home))) builder.SendKeys(OpenQA.Selenium.Keys.Home);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Insert))) builder.SendKeys(OpenQA.Selenium.Keys.Insert);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Left))) builder.SendKeys(OpenQA.Selenium.Keys.Left);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Multiply))) builder.SendKeys(OpenQA.Selenium.Keys.Multiply);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Null))) builder.SendKeys(OpenQA.Selenium.Keys.Null);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad0))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad0);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad1))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad1);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad2))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad2);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad3))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad3);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad4))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad4);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad5))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad5);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad6))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad6);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad7))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad7);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad8))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad8);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.NumberPad9))) builder.SendKeys(OpenQA.Selenium.Keys.NumberPad9);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.PageDown))) builder.SendKeys(OpenQA.Selenium.Keys.PageDown);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.PageUp))) builder.SendKeys(OpenQA.Selenium.Keys.PageUp);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Pause))) builder.SendKeys(OpenQA.Selenium.Keys.Pause);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Return))) builder.SendKeys(OpenQA.Selenium.Keys.Return);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Right))) builder.SendKeys(OpenQA.Selenium.Keys.Right);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Semicolon))) builder.SendKeys(OpenQA.Selenium.Keys.Semicolon);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Separator))) builder.SendKeys(OpenQA.Selenium.Keys.Separator);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Space))) builder.SendKeys(OpenQA.Selenium.Keys.Space);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Subtract))) builder.SendKeys(OpenQA.Selenium.Keys.Subtract);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Tab))) builder.SendKeys(OpenQA.Selenium.Keys.Tab);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Up))) builder.SendKeys(OpenQA.Selenium.Keys.Up);
            //[A-Z]
            else builder.SendKeys(key);

            builder.Build().Perform();
        }
        static void SetKeyDown(Actions builder, string key)
        {
            if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) builder.KeyDown(OpenQA.Selenium.Keys.Shift);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) builder.KeyDown(OpenQA.Selenium.Keys.Control);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) builder.KeyDown(OpenQA.Selenium.Keys.Alt);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) builder.KeyDown(OpenQA.Selenium.Keys.Meta);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) builder.KeyDown(OpenQA.Selenium.Keys.Command);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) builder.KeyDown(OpenQA.Selenium.Keys.LeftAlt);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) builder.KeyDown(OpenQA.Selenium.Keys.LeftControl);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) builder.KeyDown(OpenQA.Selenium.Keys.LeftShift);

            builder.Build().Perform();
        }
        static void SetKeyUp(Actions builder, string key)
        {
            if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) builder.KeyUp(OpenQA.Selenium.Keys.Shift);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) builder.KeyUp(OpenQA.Selenium.Keys.Control);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) builder.KeyUp(OpenQA.Selenium.Keys.Alt);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) builder.KeyUp(OpenQA.Selenium.Keys.Meta);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) builder.KeyUp(OpenQA.Selenium.Keys.Command);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) builder.KeyUp(OpenQA.Selenium.Keys.LeftAlt);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) builder.KeyUp(OpenQA.Selenium.Keys.LeftControl);
            else if (key.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) builder.KeyUp(OpenQA.Selenium.Keys.LeftShift);

            builder.Build().Perform();
        }
    }

    public class KeyData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        /// <summary>
        /// 1: Modifier; 2: Other
        /// </summary>
        public int Type { get; set; }

        static List<KeyData> lstAllKeys;
        static List<KeyData> lstModifierKeys;
        static List<KeyData> lstOtherKeys;

        public static List<KeyData> GetAllKeys()
        {
            if (lstAllKeys == null)
            {
                lstAllKeys = new List<KeyData>();
                lstAllKeys.AddRange(GetModifierKeys());
                lstAllKeys.AddRange(GetOtherKeys());
            }
            return lstAllKeys;
        }
        public static List<KeyData> GetModifierKeys()
        {
            if (lstModifierKeys == null)
            {
                lstModifierKeys = new List<KeyData>();
                List<string> lstKeys = typeof(OpenQA.Selenium.Keys).GetFields().Select(x => x.Name).OrderBy(x => x).ToList();
                lstKeys.ForEach(x =>
                {
                    if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) lstModifierKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 1 });
                });
            }
            return lstModifierKeys;
        }
        public static List<KeyData> GetOtherKeys()
        {
            if (lstOtherKeys == null)
            {
                lstOtherKeys = new List<KeyData>();
                List<string> lstKeys = typeof(OpenQA.Selenium.Keys).GetFields().Select(x => x.Name).OrderBy(x => x).ToList();
                lstKeys.ForEach(x =>
                {
                    if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Shift))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Control))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Alt))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Meta))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.Command))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftAlt))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftControl))) { }
                    else if (x.ToEqualEx(nameof(OpenQA.Selenium.Keys.LeftShift))) { }
                    else { lstOtherKeys.Add(new KeyData() { ID = x, Name = x, Value = x.ToLower(), Type = 2 }); }
                });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.A.ToString(), Name = System.Windows.Forms.Keys.A.ToString(), Value = System.Windows.Forms.Keys.A.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.B.ToString(), Name = System.Windows.Forms.Keys.B.ToString(), Value = System.Windows.Forms.Keys.B.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.C.ToString(), Name = System.Windows.Forms.Keys.C.ToString(), Value = System.Windows.Forms.Keys.C.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.D.ToString(), Name = System.Windows.Forms.Keys.D.ToString(), Value = System.Windows.Forms.Keys.D.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.E.ToString(), Name = System.Windows.Forms.Keys.E.ToString(), Value = System.Windows.Forms.Keys.E.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.F.ToString(), Name = System.Windows.Forms.Keys.F.ToString(), Value = System.Windows.Forms.Keys.F.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.G.ToString(), Name = System.Windows.Forms.Keys.G.ToString(), Value = System.Windows.Forms.Keys.G.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.H.ToString(), Name = System.Windows.Forms.Keys.H.ToString(), Value = System.Windows.Forms.Keys.H.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.I.ToString(), Name = System.Windows.Forms.Keys.I.ToString(), Value = System.Windows.Forms.Keys.I.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.J.ToString(), Name = System.Windows.Forms.Keys.J.ToString(), Value = System.Windows.Forms.Keys.J.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.K.ToString(), Name = System.Windows.Forms.Keys.K.ToString(), Value = System.Windows.Forms.Keys.K.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.L.ToString(), Name = System.Windows.Forms.Keys.L.ToString(), Value = System.Windows.Forms.Keys.L.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.M.ToString(), Name = System.Windows.Forms.Keys.M.ToString(), Value = System.Windows.Forms.Keys.M.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.N.ToString(), Name = System.Windows.Forms.Keys.N.ToString(), Value = System.Windows.Forms.Keys.N.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.O.ToString(), Name = System.Windows.Forms.Keys.O.ToString(), Value = System.Windows.Forms.Keys.O.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.P.ToString(), Name = System.Windows.Forms.Keys.P.ToString(), Value = System.Windows.Forms.Keys.P.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Q.ToString(), Name = System.Windows.Forms.Keys.Q.ToString(), Value = System.Windows.Forms.Keys.Q.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.R.ToString(), Name = System.Windows.Forms.Keys.R.ToString(), Value = System.Windows.Forms.Keys.R.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.S.ToString(), Name = System.Windows.Forms.Keys.S.ToString(), Value = System.Windows.Forms.Keys.S.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.T.ToString(), Name = System.Windows.Forms.Keys.T.ToString(), Value = System.Windows.Forms.Keys.T.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.U.ToString(), Name = System.Windows.Forms.Keys.U.ToString(), Value = System.Windows.Forms.Keys.U.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.V.ToString(), Name = System.Windows.Forms.Keys.V.ToString(), Value = System.Windows.Forms.Keys.V.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.W.ToString(), Name = System.Windows.Forms.Keys.W.ToString(), Value = System.Windows.Forms.Keys.W.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.X.ToString(), Name = System.Windows.Forms.Keys.X.ToString(), Value = System.Windows.Forms.Keys.X.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Y.ToString(), Name = System.Windows.Forms.Keys.Y.ToString(), Value = System.Windows.Forms.Keys.Y.ToString().ToLower(), Type = 2 });
                lstOtherKeys.Add(new KeyData() { ID = System.Windows.Forms.Keys.Z.ToString(), Name = System.Windows.Forms.Keys.Z.ToString(), Value = System.Windows.Forms.Keys.Z.ToString().ToLower(), Type = 2 });
            }
            return lstOtherKeys;
        }
    }

    public class KeyboardObject
    {
        public int iKeyType { get; set; } = 0;
        public string sKey { get; set; } = string.Empty;
        public string sTarget { get; set; } = string.Empty;
        public bool bPosition { get; set; } = false;
        public int iX { get; set; } = 0;
        public int iY { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
