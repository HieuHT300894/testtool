﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Drawing;
using System.Reflection;
using System.Text;

namespace SeleniumAPI
{
    public static class MouseHandle
    {
        public static void InitMouseHandle(this IWebDriver webDriver)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine("function MouseHandle(_element, _eventType) {");
            sbScript.AppendLine("    var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine("    clickEvent.initMouseEvent('_eventType', true, true);");
            sbScript.AppendLine("    _element.dispatchEvent(clickEvent);");
            sbScript.AppendLine("    console.log(_element);");
            sbScript.AppendLine("    console.log(clickEvent.type);");
            sbScript.AppendLine("}");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString());
        }
        public static void SendMouseHandle(this IWebDriver webDriver, IWebElement targetElement, string eventType)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"clickEvent.initMouseEvent('{eventType}', true, true);");
            sbScript.AppendLine($"arguments[0].dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString(), targetElement);

            //StringBuilder sbScript = new StringBuilder();
            //sbScript.AppendLine("function MouseHandle(_element, _eventType) {");
            //sbScript.AppendLine("    var clickEvent = document.createEvent('MouseEvents');");
            //sbScript.AppendLine("    clickEvent.initMouseEvent(_eventType, true, true);");
            //sbScript.AppendLine("    _element.dispatchEvent(clickEvent);");
            //sbScript.AppendLine("    console.log(_element);");
            //sbScript.AppendLine("    console.log(clickEvent.type);");
            //sbScript.AppendLine("}");
            //sbScript.AppendLine($"MouseHandle(arguments[0], '{eventType}');");

            //IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            //var a = js.ExecuteScript(sbScript.ToString(), targetElement);
        }
        public static void SendMouseHandle(this IWebDriver webDriver, string eventType, int x, int y)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"var _element = document.elementFromPoint({x}, {y});");
            sbScript.AppendLine($"clickEvent.initMouseEvent(");
            sbScript.AppendLine($"/*type*/ '{eventType}',");
            sbScript.AppendLine($"/*canBubble*/ true,");
            sbScript.AppendLine($"/*cancelable*/ true,");
            sbScript.AppendLine($"/*view*/ window,");
            sbScript.AppendLine($"/*detail*/ 0,");
            sbScript.AppendLine($"/*screenX*/ 0,");
            sbScript.AppendLine($"/*screenY*/ 0,");
            sbScript.AppendLine($"/*clientX*/ {x},");
            sbScript.AppendLine($"/*clientY*/ {y},");
            sbScript.AppendLine($"/*ctrlKey*/ false,");
            sbScript.AppendLine($"/*altKey*/ false,");
            sbScript.AppendLine($"/*shiftKey*/ false,");
            sbScript.AppendLine($"/*metaKey*/ false,");
            sbScript.AppendLine($"/*button*/ 0,");
            sbScript.AppendLine($"/*relatedTarget*/ null);");
            sbScript.AppendLine($"_element.dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString());
        }
        public static void SendMouseHandle(this IWebDriver webDriver, IWebElement targetElement, string eventType, int x, int y)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"clickEvent.initMouseEvent(");
            sbScript.AppendLine($"/*type*/ '{eventType}',");
            sbScript.AppendLine($"/*canBubble*/ true,");
            sbScript.AppendLine($"/*cancelable*/ true,");
            sbScript.AppendLine($"/*view*/ window,");
            sbScript.AppendLine($"/*detail*/ 0,");
            sbScript.AppendLine($"/*screenX*/ 0,");
            sbScript.AppendLine($"/*screenY*/ 0,");
            sbScript.AppendLine($"/*clientX*/ {x},");
            sbScript.AppendLine($"/*clientY*/ {y},");
            sbScript.AppendLine($"/*ctrlKey*/ false,");
            sbScript.AppendLine($"/*altKey*/ false,");
            sbScript.AppendLine($"/*shiftKey*/ false,");
            sbScript.AppendLine($"/*metaKey*/ false,");
            sbScript.AppendLine($"/*button*/ 0,");
            sbScript.AppendLine($"/*relatedTarget*/ null);");
            sbScript.AppendLine($"arguments[0].dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString(), targetElement);
        }
        public static void SendMouseHandle(this IWebDriver webDriver, IWebElement targetElement, string eventType, Point location)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"clickEvent.initMouseEvent(");
            sbScript.AppendLine($"/*type*/ '{eventType}',");
            sbScript.AppendLine($"/*canBubble*/ true,");
            sbScript.AppendLine($"/*cancelable*/ true,");
            sbScript.AppendLine($"/*view*/ window,");
            sbScript.AppendLine($"/*detail*/ 0,");
            sbScript.AppendLine($"/*screenX*/ 0,");
            sbScript.AppendLine($"/*screenY*/ 0,");
            sbScript.AppendLine($"/*clientX*/ {location.X},");
            sbScript.AppendLine($"/*clientY*/ {location.Y},");
            sbScript.AppendLine($"/*ctrlKey*/ false,");
            sbScript.AppendLine($"/*altKey*/ false,");
            sbScript.AppendLine($"/*shiftKey*/ false,");
            sbScript.AppendLine($"/*metaKey*/ false,");
            sbScript.AppendLine($"/*button*/ 0,");
            sbScript.AppendLine($"/*relatedTarget*/ null);");
            sbScript.AppendLine($"arguments[0].dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString(), targetElement);
        }
        public static Func<string> GetFunc(MouseObject obj)
        {
            if (obj.bPosition)
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                if (element != null)
                                {
                                    DOMRect rect = element.GetElementRect();
                                    Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                    if (obj.sHandle.ToEqualEx(Define.Instance.Click))
                                    {
                                        //builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Click().Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.Click, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.DblClick))
                                    {
                                        //builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.Click, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                        wd.SendMouseHandle(element, Define.Instance.Click, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseDown))
                                    {
                                        //builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.MouseDown, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseMove))
                                    {
                                        //    builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.MouseMove, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseUp))
                                    {
                                        //builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.MouseUp, element.Location.X + obj.iX, element.Location.Y + obj.iY);

                                        //RemoteWebDriver rwd = wd as RemoteWebDriver;
                                        //RemoteWebElement rwe = element as RemoteWebElement;
                                        //RemoteTouchScreen rts = new RemoteTouchScreen(rwd);
                                        //rts.Scroll(rwe.Coordinates, 100, 100);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseOver))
                                    {
                                        //builder.MoveToElement(element, element.Location.X + iX, element.Location.Y + iY).Build().Perform();
                                        wd.SendMouseHandle(element, Define.Instance.MouseOver, element.Location.X + obj.iX, element.Location.Y + obj.iY);
                                    }
                                }
                                else
                                {
                                    if (obj.sHandle.ToEqualEx(Define.Instance.Click))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Click().Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.Click, obj.iX, obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.DblClick))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.Click, obj.iX, obj.iY);
                                        wd.SendMouseHandle(Define.Instance.Click, obj.iX, obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseDown))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.MouseDown, obj.iX, obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseMove))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.MouseMove, obj.iX, obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseUp))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.MouseUp, obj.iX, obj.iY);
                                    }
                                    else if (obj.sHandle.ToEqualEx(Define.Instance.MouseOver))
                                    {
                                        builder.MoveByOffset(obj.iX, obj.iY).Build().Perform();
                                        wd.SendMouseHandle(Define.Instance.MouseOver, obj.iX, obj.iY);
                                    }

                                }
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
            else
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                DOMRect rect = element.GetElementRect();
                                Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                if (obj.sHandle.ToEqualEx(Define.Instance.Click))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.Click);
                                }
                                else if (obj.sHandle.ToEqualEx(Define.Instance.DblClick))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.Click);
                                    wd.SendMouseHandle(element, Define.Instance.Click);
                                }
                                else if (obj.sHandle.ToEqualEx(Define.Instance.MouseDown))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.MouseDown);
                                }
                                else if (obj.sHandle.ToEqualEx(Define.Instance.MouseMove))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.MouseMove);
                                }
                                else if (obj.sHandle.ToEqualEx(Define.Instance.MouseUp))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.MouseUp);
                                }
                                else if (obj.sHandle.ToEqualEx(Define.Instance.MouseOver))
                                {
                                    builder.MoveToElement(element).Build().Perform();
                                    wd.SendMouseHandle(element, Define.Instance.MouseOver);
                                }
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
        }
    }

    public class MouseObject
    {
        public string sHandle { get; set; } = string.Empty;
        public string sTarget { get; set; } = string.Empty;
        public bool bPosition { get; set; } = false;
        public int iX { get; set; } = 0;
        public int iY { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
