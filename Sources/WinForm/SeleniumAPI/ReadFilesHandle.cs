﻿using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class ReadFilesHandle
    {
        public static Func<string> GetFunc(ReadFilesObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        try
                        {
                            if (obj.iReadType == Define.eReadType.First)
                            {
                                string text = string.Empty;
                                if (Extension.ReadFirst(obj.sPath, ref text))
                                {
                                    List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
                                    if (text.SplitRegex(obj.sRegex, items))
                                    {
                                        foreach (KeyValuePair<string, string> item1 in obj.selectedValues)
                                        {
                                            KeyValuePair<string, string> item2 = items.FirstOrDefault(x => x.Key.ToEqualEx(item1.Key));
                                            if (!item1.Value.ToEqualEx(item2.Value))
                                                return false;
                                        }
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else if (obj.iReadType == Define.eReadType.Last)
                            {
                                string text = string.Empty;
                                if (Extension.ReadLast(obj.sPath, ref text))
                                {
                                    List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
                                    if (text.SplitRegex(obj.sRegex, items))
                                    {
                                        foreach (KeyValuePair<string, string> item1 in obj.selectedValues)
                                        {
                                            KeyValuePair<string, string> item2 = items.FirstOrDefault(x => x.Key.ToEqualEx(item1.Key));
                                            if (!item1.Value.ToEqualEx(item2.Value))
                                                return false;
                                        }
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class ReadFilesObject
    {
        public Define.eReadType iReadType { get; set; } = Define.eReadType.None;
        public string sPath { get; set; } = string.Empty;
        public string sRegex { get; set; } = string.Empty;
        public string sValue { get; set; } = string.Empty;
        public int iStart { get; set; } = 0;
        public int iEnd { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
        public List<KeyValuePair<string, string>> selectedValues { get; set; } = new List<KeyValuePair<string, string>>();
    }
}
