﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public class ScrollHandle
    {
        public static Func<string> GetFunc(ScrollObject obj)
        {
            if (obj.bPosition)
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                if (element != null)
                                {
                                    DOMRect rect = element.GetElementRect();
                                    Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                    RemoteWebDriver rwd = wd as RemoteWebDriver;
                                    RemoteWebElement rwe = element as RemoteWebElement;
                                    RemoteTouchScreen rts = new RemoteTouchScreen(rwd);
                                    rts.Scroll(rwe.Coordinates, obj.iX, obj.iY);
                                }
                                else
                                {
                                    RemoteWebDriver rwd = wd as RemoteWebDriver;
                                    RemoteTouchScreen rts = new RemoteTouchScreen(rwd);
                                    rts.Scroll(obj.iX, obj.iY);
                                }
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
            else
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                DOMRect rect = element.GetElementRect();
                                Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                RemoteWebDriver rwd = wd as RemoteWebDriver;
                                RemoteWebElement rwe = element as RemoteWebElement;
                                RemoteTouchScreen rts = new RemoteTouchScreen(rwd);
                                rts.Scroll(rect.X, rect.Y);

                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
        }
    }

    public class ScrollObject
    {
        public string sTarget { get; set; } = string.Empty;
        public bool bPosition { get; set; } = false;
        public int iX { get; set; } = 0;
        public int iY { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
