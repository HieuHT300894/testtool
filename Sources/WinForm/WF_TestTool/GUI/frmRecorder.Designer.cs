﻿namespace WF_TestTool.GUI
{
    partial class frmRecorder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.lbMacro = new System.Windows.Forms.ListBox();
            this.lbHandle = new System.Windows.Forms.ListBox();
            this.lbXPath = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.btnStart, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnStop, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPlay, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbMacro, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbHandle, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbXPath, 3, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 411);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(23, 23);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 25);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(129, 23);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 25);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(235, 23);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(100, 25);
            this.btnPlay.TabIndex = 2;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            // 
            // lbMacro
            // 
            this.lbMacro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMacro.FormattingEnabled = true;
            this.lbMacro.Location = new System.Drawing.Point(23, 54);
            this.lbMacro.Name = "lbMacro";
            this.lbMacro.Size = new System.Drawing.Size(100, 334);
            this.lbMacro.TabIndex = 3;
            // 
            // lbHandle
            // 
            this.lbHandle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHandle.FormattingEnabled = true;
            this.lbHandle.Location = new System.Drawing.Point(129, 54);
            this.lbHandle.Name = "lbHandle";
            this.lbHandle.Size = new System.Drawing.Size(100, 334);
            this.lbHandle.TabIndex = 4;
            // 
            // lbXPath
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.lbXPath, 2);
            this.lbXPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbXPath.FormattingEnabled = true;
            this.lbXPath.Location = new System.Drawing.Point(235, 54);
            this.lbXPath.Name = "lbXPath";
            this.lbXPath.Size = new System.Drawing.Size(626, 334);
            this.lbXPath.TabIndex = 5;
            // 
            // frmRecorder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 411);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frmRecorder";
            this.Text = "frmRecorder";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.ListBox lbMacro;
        private System.Windows.Forms.ListBox lbHandle;
        private System.Windows.Forms.ListBox lbXPath;
    }
}