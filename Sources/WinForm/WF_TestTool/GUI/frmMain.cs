﻿using System;
using System.Windows.Forms;

namespace WF_TestTool.GUI
{
    public partial class frmMain : frmBase
    {
        frmEditor frmEditor = null;
        frmExecution frmExecution = null;

        public frmMain()
        {
            InitializeComponent();
        }

        protected override void FrmBase_Load(object sender, EventArgs e)
        {
            base.FrmBase_Load(sender, e);

            btnEditor.Click += BtnEditor_Click;
            btnExecution.Click += BtnExecution_Click;
        }
        protected override void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            base.FrmBase_FormClosing(sender, e);

            if (frmEditor != null)
                frmEditor.Close();
            if (frmExecution != null)
                frmExecution.Close();
        }

        private void BtnExecution_Click(object sender, EventArgs e)
        {
            if (frmExecution != null)
                frmExecution.Close();

            frmExecution = new frmExecution();
            frmExecution.showFormMain = ShowForm;
            frmExecution.Show();

            HideForm();
        }
        private void BtnEditor_Click(object sender, EventArgs e)
        {
            if (frmEditor != null)
                frmEditor.Close();

            frmEditor = new frmEditor();
            frmEditor.showFormMain = ShowForm;
            frmEditor.Show();

            HideForm();
        }

        void ShowForm()
        {
            Show();
        }
        void HideForm()
        {
            Hide();
        }
    }
}
