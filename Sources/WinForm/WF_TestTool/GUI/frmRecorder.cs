﻿using Common;
using MouseKeyboardLibrary;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using WF_TestTool.Model;

namespace WF_TestTool.GUI
{
    public partial class frmRecorder : frmBase
    {
        MouseHook mouseHook = null;
        KeyboardHook keyboardHook = null;
        BindingList<MacroEvent> macroes = null;
        BindingList<MacroEvent> handles = null;
        List<MacroEvent> stack = null;
        //IWebDriver driver = null;
        MacroEvent lastMacro = null;
        Timer timer = null;
        bool isRecord = false;
        int lastTimeTickCount = 0;
        int lastTime = 0;
        bool isMouseDown = false;
        bool isMouseUp = true;

        public frmRecorder()
        {
            InitializeComponent();
        }

        protected override void FrmBase_Load(object sender, EventArgs e)
        {
            base.FrmBase_Load(sender, e);
            try
            {
                mouseHook = new MouseHook();
                keyboardHook = new KeyboardHook();
                macroes = new BindingList<MacroEvent>();
                handles = new BindingList<MacroEvent>();
                stack = new List<MacroEvent>();
                timer = new Timer() { Interval = 250 };
                ChromeHandle.StartChromeDriver();
                ChromeHandle.GoToUrl("http://localhost/LSApiTest/WebEditTest.htm");
                //ChromeHandle.GoToUrl("http://w2016-container.japaneast.cloudapp.azure.com/lsapitest/WebEditTest.htm");
                //mouseHook.Click -= mouseHook_Click;
                mouseHook.DoubleClick -= mouseHook_DoubleClick;
                mouseHook.MouseMove -= mouseHook_MouseMove;
                mouseHook.MouseDown -= mouseHook_MouseDown;
                mouseHook.MouseUp -= mouseHook_MouseUp;
                mouseHook.MouseWheel -= mouseHook_MouseWheel;
                keyboardHook.KeyDown -= keyboardHook_KeyDown;
                keyboardHook.KeyUp -= keyboardHook_KeyUp;
                keyboardHook.KeyPress -= keyboardHook_KeyPress;
                btnStart.Click -= BtnStart_Click;
                btnStop.Click -= BtnStop_Click;
                btnPlay.Click -= BtnPlay_Click;
                timer.Tick -= Timer_Tick;

                //mouseHook.Click += mouseHook_Click;
                mouseHook.DoubleClick += mouseHook_DoubleClick;
                mouseHook.MouseMove += mouseHook_MouseMove;
                mouseHook.MouseDown += mouseHook_MouseDown;
                mouseHook.MouseUp += mouseHook_MouseUp;
                mouseHook.MouseWheel += mouseHook_MouseWheel;
                keyboardHook.KeyDown += keyboardHook_KeyDown;
                keyboardHook.KeyUp += keyboardHook_KeyUp;
                keyboardHook.KeyPress += keyboardHook_KeyPress;
                btnStart.Click += BtnStart_Click;
                btnStop.Click += BtnStop_Click;
                btnPlay.Click += BtnPlay_Click;
                timer.Tick += Timer_Tick;

                lbMacro.DataSource = macroes;
                lbHandle.DataSource = handles;

                lbMacro.FormatEx("Text", "Text");
                lbHandle.FormatEx("Text", "Text");

                mouseHook.Start();
                keyboardHook.Start();
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                this.showError(Caption: ex.Message);
            }
        }
        protected override void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            base.FrmBase_FormClosing(sender, e);

            try
            {
                if (mouseHook != null)
                    mouseHook.Stop();
                if (keyboardHook != null)
                    keyboardHook.Stop();
                ChromeHandle.ShutdownChromeDriver();
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                this.showError(Caption: ex.Message);
            }
        }

        private async void BtnPlay_Click(object sender, EventArgs e)
        {
            if (isRecord)
                return;

            await Task.Factory.StartNew(() =>
            {
                Invoke(new Action(() =>
                {
                    lbHandle.SelectedIndex = -1;
                    lbXPath.SelectedIndex = -1;
                }));
            });

            for (int i = 0; i < handles.Count; i++)
            {
                Action act = null;
                MacroEvent macroEvent = handles[i];

                switch (macroEvent.MacroEventType)
                {
                    case MacroEventType.Click:
                        {
                            act = new Action(() =>
                            {
                                MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;
                                MouseSimulator.Click(mouseArgs);
                            });
                            break;
                        }
                    case MacroEventType.DoubleClick:
                        {
                            act = new Action(() =>
                            {
                                MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;
                                MouseSimulator.DoubleClick(mouseArgs);
                            });
                            break;
                        }
                    case MacroEventType.MouseMove:
                        {
                            act = new Action(() =>
                            {
                                MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;
                                MouseSimulator.MouseMove(mouseArgs);
                            });
                        }
                        break;
                    case MacroEventType.MouseDown:
                        {
                            act = new Action(() =>
                            {
                                MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;
                                MouseSimulator.MouseDown(mouseArgs);
                            });
                        }
                        break;
                    case MacroEventType.MouseUp:
                        {
                            act = new Action(() =>
                            {
                                MouseEventArgs mouseArgs = (MouseEventArgs)macroEvent.EventArgs;
                                MouseSimulator.MouseUp(mouseArgs);
                            });
                        }
                        break;
                    case MacroEventType.KeyDown:
                        {
                            act = new Action(() =>
                            {
                                KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;
                                KeyboardSimulator.KeyDown(keyArgs.KeyCode);
                            });
                        }
                        break;
                    case MacroEventType.KeyUp:
                        {
                            act = new Action(() =>
                            {
                                KeyEventArgs keyArgs = (KeyEventArgs)macroEvent.EventArgs;
                                KeyboardSimulator.KeyUp(keyArgs.KeyCode);
                            });
                        }
                        break;
                    default:
                        {
                            act = new Action(() =>
                            {
                            });
                        }
                        break;
                }
                await Task.Factory.StartNew(() => { Invoke(new Action(() => { lbHandle.SelectedIndex = i; lbXPath.SelectedIndex = i; })); });
                await Task.Factory.StartNew(act);
                await Task.Delay(macroEvent.TimePeriod);
            }
        }
        private void BtnStop_Click(object sender, EventArgs e)
        {
            if (!isRecord)
                return;

            isRecord = false;
            timer.Stop();
        }
        private void BtnStart_Click(object sender, EventArgs e)
        {
            if (isRecord)
                return;

            isRecord = true;
            lastMacro = null;
            lastTimeTickCount = Environment.TickCount;
            lastTime = 0;
            timer.Start();

            macroes.Clear();
            handles.Clear();
            lbXPath.Items.Clear();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Tick -= Timer_Tick;
            timer.Stop();
            if (lastMacro != null)
                InsertHandle(lastMacro);
            lastMacro = null;
            timer.Start();
            timer.Tick += Timer_Tick;
        }

        private void keyboardHook_KeyPress(object sender, KeyPressEventArgs e)
        {
            AddKeyboardEvent(e, MacroEventType.KeyPress);
        }
        private void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            AddKeyboardEvent(e, MacroEventType.KeyUp);
        }
        private void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            AddKeyboardEvent(e, MacroEventType.KeyDown);
        }

        private void mouseHook_Click(object sender, EventArgs e)
        {
            AddMouseEvent((MouseEventArgs)e, MacroEventType.Click);
        }
        private void mouseHook_DoubleClick(object sender, EventArgs e)
        {
            AddMouseEvent((MouseEventArgs)e, MacroEventType.DoubleClick);
        }
        private void mouseHook_MouseWheel(object sender, MouseEventArgs e)
        {
            AddMouseEvent(e, MacroEventType.MouseWheel);
        }
        private void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            AddMouseEvent(e, MacroEventType.MouseUp);
        }
        private void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            AddMouseEvent(e, MacroEventType.MouseDown);
        }
        private void mouseHook_MouseMove(object sender, MouseEventArgs e)
        {
            SetXYLabel(e.X, e.Y);
            AddMouseEvent(e, MacroEventType.MouseMove);
        }

        void SetXYLabel(int x, int y)
        {
            Text = string.Format("Current Mouse Point: X={0}, y={1}", x, y);
        }
        void AddMouseEvent(EventArgs e, MacroEventType eventType)
        {
            if (!isRecord)
                return;

            MacroEvent macro = CreateMacro(e, eventType);
            macroes.Add(macro);
            switch (macro.MacroEventType)
            {
                case MacroEventType.Click:
                    InsertHandle(macro);
                    break;
                case MacroEventType.DoubleClick:
                    InsertHandle(macro);
                    break;
                case MacroEventType.MouseDown:
                    isMouseDown = true;
                    isMouseUp = false;
                    stack.Clear();
                    stack.Add(macro);
                    break;
                case MacroEventType.MouseUp:
                    isMouseDown = false;
                    isMouseUp = true;
                    stack.Add(macro);
                    CheckClickOrDblClick(macro);
                    break;
                case MacroEventType.MouseMove:
                    if (isMouseDown && !isMouseUp)
                        stack.Add(macro);
                    else
                        InsertHandle(macro);
                    break;
            }
        }
        void AddKeyboardEvent(EventArgs e, MacroEventType eventType)
        {
            if (!isRecord)
                return;

            MacroEvent macro = CreateMacro(e, eventType);
            macroes.Add(macro);
            InsertHandle(macro);
        }
        MacroEvent CreateMacro(EventArgs e, MacroEventType eventType)
        {
            MacroEvent macro = new MacroEvent();
            macro.Name = eventType.ToString();
            macro.Text = eventType.ToString();
            macro.MacroEventType = eventType;
            macro.EventArgs = e;
            macro.TimePeriod = Environment.TickCount - lastTimeTickCount;
            macro.TimeLast = lastTime + macro.TimePeriod;

            lastTime = macro.TimeLast;
            lastTimeTickCount = Environment.TickCount;

            return macro;
        }
        void CheckClickOrDblClick(MacroEvent mouseClick)
        {
            int iMouseDown = 0;
            int iMouseMove = 0;
            int iMouseUp = 0;

            MacroEvent mouseDown = null;
            MacroEvent mouseMove_Start = null;
            MacroEvent mouseMove_End = null;
            MacroEvent mouseUp = null;

            foreach (MacroEvent macro in stack)
            {
                if (macro.MacroEventType == MacroEventType.MouseDown)
                {
                    iMouseDown++;
                    if (mouseDown == null)
                    {
                        mouseDown = macro;
                    }
                }
                else if (macro.MacroEventType == MacroEventType.MouseMove)
                {
                    iMouseMove++;
                    if (mouseMove_Start == null)
                        mouseMove_Start = macro;
                    mouseMove_End = macro;
                }
                else if (macro.MacroEventType == MacroEventType.MouseUp)
                {
                    iMouseUp++;
                    mouseUp = macro;
                }
            }

            if (iMouseDown == 1 && iMouseMove == 0 && iMouseUp == 1)
            {
                MouseEventArgs mouse = (MouseEventArgs)mouseClick.EventArgs;
                if (lastMacro == null)
                {
                    lastMacro = CreateMacro(new MouseEventArgs(mouse.Button, 1, mouse.X, mouse.Y, mouse.Delta), MacroEventType.Click);
                    //AddMouseEvent(new MouseEventArgs(mouse.Button, 1, mouse.X, mouse.Y, mouse.Delta), MacroEventType.Click);
                    timer.Stop();
                    timer.Start();
                }
                else
                {
                    if ((mouseClick.TimeLast - lastMacro.TimeLast) < timer.Interval)
                    {
                        AddMouseEvent(new MouseEventArgs(mouse.Button, 2, mouse.X, mouse.Y, mouse.Delta), MacroEventType.DoubleClick);
                    }
                    lastMacro = null;
                }
            }
            else if (iMouseDown == 1 && iMouseMove > 0 && iMouseUp == 1)
            {
                //Mouse down
                InsertHandle(mouseDown);

                //Mouse move
                InsertHandle(mouseMove_Start);
                if (iMouseMove > 1)
                    InsertHandle(mouseMove_End);

                //Mouse up
                InsertHandle(mouseUp);
            }
        }
        void InsertHandle(MacroEvent macro)
        {
            switch (macro.MacroEventType)
            {
                case MacroEventType.Click:
                case MacroEventType.DoubleClick:
                case MacroEventType.MouseMove:
                case MacroEventType.MouseDown:
                case MacroEventType.MouseUp:
                    MouseEventArgs mouse = (MouseEventArgs)macro.EventArgs;
                    lbXPath.Items.Add(XPathGenerate.GetXpath(mouse.Location) + ChromeHandle.GetElementRect(mouse.X, mouse.Y).SerializeObjectToJson());
                    break;
                case MacroEventType.KeyDown:
                case MacroEventType.KeyUp:
                    KeyEventArgs keyArgs = (KeyEventArgs)macro.EventArgs;
                    break;
                default:
                    break;
            }
            handles.Add(macro);
        }
    }
}