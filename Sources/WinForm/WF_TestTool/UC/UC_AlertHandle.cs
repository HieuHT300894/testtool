﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using Common;
using SeleniumAPI;

namespace WF_TestTool.UC
{
    public partial class UC_AlertHandle : UC_Base
    {
        public UC_AlertHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            rbtnSimple.CheckedChanged -= RbtnSimple_CheckedChanged;
            rbtnPrompt.CheckedChanged -= RbtnPrompt_CheckedChanged;
            rbtnConfirmation.CheckedChanged -= RbtnConfirmation_CheckedChanged;

            rbtnSimple.CheckedChanged += RbtnSimple_CheckedChanged;
            rbtnPrompt.CheckedChanged += RbtnPrompt_CheckedChanged;
            rbtnConfirmation.CheckedChanged += RbtnConfirmation_CheckedChanged;
        }

        private void RbtnConfirmation_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (!rbtn.Checked)
                return;

            lbInput.Visible = false;
            txtInput.Visible = false;
            rbtnCancel.Visible = true;
        }
        private void RbtnPrompt_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (!rbtn.Checked)
                return;

            lbInput.Visible = true;
            txtInput.Visible = true;
            rbtnCancel.Visible = true;
        }
        private void RbtnSimple_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (!rbtn.Checked)
                return;

            if (rbtnCancel.Checked)
                rbtnOk.Checked = true;
            lbInput.Visible = false;
            txtInput.Visible = false;
            rbtnCancel.Visible = false;
        }

        public void GetData(AlertObject obj)
        {
            if (rbtnSimple.Checked)
                obj.sAlertType = Define.eAlertType.SimpleAlert;
            else if (rbtnPrompt.Checked)
                obj.sAlertType = Define.eAlertType.PromptAlert;
            else if (rbtnConfirmation.Checked)
                obj.sAlertType = Define.eAlertType.ConfirmationAlert;

            if (rbtnOk.Checked)
                obj.sButtonType = Define.eAlertType.OK;
            else if (rbtnCancel.Checked)
                obj.sButtonType = Define.eAlertType.Cancel;

            obj.sText = txtText.Text.Trim();
            obj.sInput = txtInput.Visible ? txtInput.Text.Trim() : string.Empty;
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            AlertObject obj = new AlertObject();
            GetData(obj);

            return AlertHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            txtInput.Format();
            txtText.Format();
            numTimeout.Format();

            Define.eAlertType sAlertType = Define.eAlertType.None;
            Define.eAlertType sButtonType = Define.eAlertType.None;
            string sText = string.Empty;
            string sInput = string.Empty;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.AlertType] != null)
                    Enum.TryParse(xmlInput.Attributes[Define.Instance.AlertType].Value, out sAlertType);
                if (xmlInput.Attributes[Define.Instance.Alert_Text] != null)
                    sText = xmlInput.Attributes[Define.Instance.AlertText].Value;
                if (xmlInput.Attributes[Define.Instance.AlertInput] != null)
                    sInput = xmlInput.Attributes[Define.Instance.AlertInput].Value;
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
                if (xmlInput.Attributes[Define.Instance.ButtonType] != null)
                    Enum.TryParse(xmlInput.Attributes[Define.Instance.ButtonType].Value, out sButtonType);
            }

            txtText.Text = sText;
            txtInput.Text = sInput;
            numTimeout.Value = iTimeout;

            if (sAlertType == Define.eAlertType.SimpleAlert) { rbtnSimple.Checked = true; }
            else if (sAlertType == Define.eAlertType.PromptAlert) { rbtnPrompt.Checked = true; }
            else if (sAlertType == Define.eAlertType.ConfirmationAlert) { rbtnConfirmation.Checked = true; }
            else { rbtnSimple.Checked = true; }

            if (sButtonType == Define.eAlertType.OK) { rbtnOk.Checked = true; }
            else if (sButtonType == Define.eAlertType.Cancel) { rbtnCancel.Checked = true; }
            else { rbtnOk.Checked = true; }

            RbtnSimple_CheckedChanged(rbtnSimple, new EventArgs());
            RbtnPrompt_CheckedChanged(rbtnPrompt, new EventArgs());
            RbtnConfirmation_CheckedChanged(rbtnConfirmation, new EventArgs());
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            AlertObject obj = new AlertObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Alert);
            attrs.Add(Define.Instance.AlertText, obj.sText);
            attrs.Add(Define.Instance.AlertInput, obj.sInput);
            attrs.Add(Define.Instance.AlertType, (int)obj.sAlertType);
            attrs.Add(Define.Instance.ButtonType, (int)obj.sButtonType);
            attrs.Add(Define.Instance.Timeout, obj.iTimeout.ToString());
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}