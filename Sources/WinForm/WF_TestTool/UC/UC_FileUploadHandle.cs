﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using WF_TestTool.GUI;

namespace WF_TestTool.UC
{
    public partial class UC_UploadFileHandle : UC_Base
    {
        public UC_UploadFileHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            btnLoad.Click -= BtnLoad_Click;
            btnEdit.Click -= BtnEdit_Click;
            txtPath.TextChanged -= TxtPath_TextChanged;

            btnLoad.Click += BtnLoad_Click;
            btnEdit.Click += BtnEdit_Click;
            txtPath.TextChanged += TxtPath_TextChanged;
        }

        private void TxtPath_TextChanged(object sender, EventArgs e)
        {
            txtPath.TextChanged -= TxtPath_TextChanged;

            if (txtPath.Text.IsNotEmpty())
                txtPath.Text = Extension.ShortcutPath(txtPath.Text.Trim('"'));

            txtPath.TextChanged += TxtPath_TextChanged;
        }
        private void BtnEdit_Click(object sender, EventArgs e)
        {
            frmFileEditor frm = new frmFileEditor(txtPath.Text.TrimStart(Path.DirectorySeparatorChar));
            frm.sendPath = new frmFileEditor.SendPath(new Action<string>((path) => { txtPath.Text = path; }));
            frm.ShowDialog(this);
        }
        private void BtnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = Define.Instance.filterAllFiles;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = dialog.FileName;
            }
        }

        public void GetData(UploadObject obj)
        {
            obj.sTarget = txtTarget.Text.Trim();
            obj.sPath = txtPath.Text.Trim();
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            UploadObject obj = new UploadObject();
            GetData(obj);

            return UploadHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            txtPath.Format();
            txtTarget.Format();
            numTimeout.Format();

            string sTarget = string.Empty;
            string sPath = string.Empty;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.Target] != null)
                    sTarget = xmlInput.Attributes[Define.Instance.Target].Value;
                if (xmlInput.Attributes[Define.Instance.Path] != null)
                    sPath = xmlInput.Attributes[Define.Instance.Path].Value;
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            txtTarget.Text = sTarget;
            txtPath.Text = sPath;
            numTimeout.Value = iTimeout;
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            UploadObject obj = new UploadObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Upload);
            attrs.Add(Define.Instance.Target, obj.sTarget);
            attrs.Add(Define.Instance.Path, obj.sPath);
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}