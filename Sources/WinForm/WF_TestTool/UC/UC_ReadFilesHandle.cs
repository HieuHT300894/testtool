﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using WF_TestTool.GUI;

namespace WF_TestTool.UC
{
    public partial class UC_ReadFilesHandle : UC_Base
    {
        public UC_ReadFilesHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            btnLoad.Click -= BtnLoad_Click;
            btnEdit.Click -= BtnEdit_Click;
            txtPath.TextChanged -= TxtPath_TextChanged;
            txtRegex.TextChanged -= Txt_TextChanged;
            txtValue.TextChanged -= Txt_TextChanged;
            rbtnCustom.CheckedChanged -= RbtnCustom_CheckedChanged;
            rbtnFirst.CheckedChanged -= Rbtn_CheckedChanged;
            rbtnLast.CheckedChanged -= Rbtn_CheckedChanged;
            cMenu_Word.Click -= CMenu_Word_Click;
            lvWords.MouseDown -= LvWords_MouseDown;

            btnLoad.Click += BtnLoad_Click;
            btnEdit.Click += BtnEdit_Click;
            txtPath.TextChanged += TxtPath_TextChanged;
            txtRegex.TextChanged += Txt_TextChanged;
            txtValue.TextChanged += Txt_TextChanged;
            rbtnCustom.CheckedChanged += RbtnCustom_CheckedChanged;
            rbtnFirst.CheckedChanged += Rbtn_CheckedChanged;
            rbtnLast.CheckedChanged += Rbtn_CheckedChanged;
            cMenu_Word.Click += CMenu_Word_Click;
            lvWords.MouseDown += LvWords_MouseDown;
        }

        private void LvWords_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                cMenu_Delete.Enabled = false;
                if (lvWords.GetItemAt(e.Location.X, e.Location.Y) != null)
                    cMenu_Delete.Enabled = true;
            }
        }
        private void CMenu_Word_Click(object sender, EventArgs e)
        {
            for (int i = lvWords.SelectedItems.Count - 1; i >= 0; i--)
            {
                lvWords.Items.Remove(lvWords.SelectedItems[i]);
            }
        }
        private void Rbtn_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (rbtn.Checked)
            {
                numStart.Enabled = false;
                numEnd.Enabled = false;
            }
        }
        private void RbtnCustom_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (rbtn.Checked)
            {
                numStart.Enabled = true;
                numEnd.Enabled = true;
            }
        }
        private void TxtPath_TextChanged(object sender, EventArgs e)
        {
            txtPath.TextChanged -= TxtPath_TextChanged;

            if (txtPath.Text.IsNotEmpty())
                txtPath.Text = Extension.ShortcutPath(txtPath.Text);
            ExchangeValue();

            txtPath.TextChanged += TxtPath_TextChanged;
        }
        private void Txt_TextChanged(object sender, EventArgs e)
        {
            ExchangeValue();
        }
        private void BtnEdit_Click(object sender, EventArgs e)
        {
            frmFileEditor frm = new frmFileEditor(txtPath.Text);
            frm.sendPath = new frmFileEditor.SendPath(new Action<string>((path) => { txtPath.Text = path; }));
            frm.ShowDialog(this);
        }
        private void BtnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = Define.Instance.filterAllFiles;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = dialog.FileName;
            }
        }

        public void GetData(ReadFilesObject obj)
        {
            if (rbtnFirst.Checked)
                obj.iReadType = Define.eReadType.First;
            else if (rbtnLast.Checked)
                obj.iReadType = Define.eReadType.Last;
            else if (rbtnCustom.Checked)
            {
                obj.iReadType = Define.eReadType.Custom;
                obj.iStart = Convert.ToInt32(numStart.Value);
                obj.iEnd = Convert.ToInt32(numEnd.Value);
            }

            obj.sPath = txtPath.Text.Trim();
            obj.sRegex = txtRegex.Text.Trim();
            obj.sValue = txtValue.Text.Trim();
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);

            List<ListViewItem> selectedItems = lvWords.GetItems();
            foreach (ListViewItem item in selectedItems)
            {
                KeyValuePair<string, string> _item = (KeyValuePair<string, string>)item.Tag;
                obj.selectedValues.Add(new KeyValuePair<string, string>(_item.Key, _item.Value));
            }
        }
        public override Func<string> GetFunc()
        {
            ReadFilesObject obj = new ReadFilesObject();
            GetData(obj);

            return ReadFilesHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            lvWords.Columns.Add("colValue", "Value", lvWords.Width);

            txtPath.Format(height: 25);
            txtRegex.Format();
            txtValue.Format();
            numStart.Format();
            numEnd.Format();
            numTimeout.Format();
            lvWords.Format();

            Define.eReadType iReadType = Define.eReadType.None;
            string sPath = string.Empty;
            string sRegex = string.Empty;
            string sValue = string.Empty;
            int iStart = 0;
            int iEnd = 0;
            int iTimeout = Define.Instance.TimeoutValue;
            List<KeyValuePair<int, string>> selectedValues = new List<KeyValuePair<int, string>>();

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.ReadType] != null)
                    Enum.TryParse(xmlInput.Attributes[Define.Instance.ReadType].Value, out iReadType);
                if (xmlInput.Attributes[Define.Instance.Path] != null)
                    sPath = xmlInput.Attributes[Define.Instance.Path].Value;
                if (xmlInput.Attributes[Define.Instance.Regex] != null)
                    sRegex = xmlInput.Attributes[Define.Instance.Regex].Value;
                if (xmlInput.Attributes[Define.Instance.Value] != null)
                    sValue = xmlInput.Attributes[Define.Instance.Value].Value;
                if (xmlInput.Attributes[Define.Instance.StartLine] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.StartLine].Value, out iStart);
                if (xmlInput.Attributes[Define.Instance.EndLine] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.EndLine].Value, out iEnd);
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);

                XmlNodeList xmlOptions = xmlInput.SelectNodes(Define.Instance.Option);
                foreach (XmlNode xmlOption in xmlOptions)
                {
                    string _sIndex = string.Empty;
                    string _sValue = string.Empty;

                    if (xmlOption.Attributes[Define.Instance.Index] != null)
                        _sIndex = xmlOption.Attributes[Define.Instance.Index].Value;
                    if (xmlOption.Attributes[Define.Instance.Value] != null)
                        _sValue = xmlOption.Attributes[Define.Instance.Value].Value;

                    ListViewItem lvItem = new ListViewItem(_sValue);
                    lvItem.Tag = new KeyValuePair<string, string>(_sIndex, _sValue);
                    lvWords.Items.Add(lvItem);
                }
            }

            txtRegex.Text = sRegex;
            txtPath.Text = sPath;
            txtValue.Text = sValue;
            numStart.Value = iStart;
            numEnd.Value = iEnd;
            numTimeout.Value = iTimeout;

            if (iReadType == Define.eReadType.First)
            {
                rbtnFirst.Checked = true;
                Rbtn_CheckedChanged(rbtnFirst, new EventArgs());
            }
            else if (iReadType == Define.eReadType.Last)
            {
                rbtnLast.Checked = true;
                Rbtn_CheckedChanged(rbtnLast, new EventArgs());
            }
            else if (iReadType == Define.eReadType.Custom)
            {
                rbtnCustom.Checked = true;
                RbtnCustom_CheckedChanged(rbtnCustom, new EventArgs());
            }
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            ReadFilesObject obj = new ReadFilesObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.ReadFile);
            attrs.Add(Define.Instance.ReadType, (int)obj.iReadType);
            attrs.Add(Define.Instance.Path, obj.sPath);
            attrs.Add(Define.Instance.Regex, obj.sRegex);
            attrs.Add(Define.Instance.Value, obj.sValue);
            if (obj.iReadType == Define.eReadType.Custom)
            {
                attrs.Add(Define.Instance.StartLine, obj.iStart);
                attrs.Add(Define.Instance.EndLine, obj.iEnd);
            }
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            XmlNode xmlInput = xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);

            foreach (KeyValuePair<string, string> item in obj.selectedValues)
            {
                Dictionary<string, object> attrOptions = new Dictionary<string, object>();
                attrOptions.Add(Define.Instance.Index, item.Key);
                attrOptions.Add(Define.Instance.Value, item.Value);
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Option, attrOptions);
            }
        }
        void ExchangeValue()
        {
            lvWords.Items.Clear();

            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
            if (Extension.SplitRegex(txtValue.Text, txtRegex.Text, items))
            {
                foreach (KeyValuePair<string, string> item in items)
                {
                    ListViewItem lvItem = new ListViewItem(item.Value);
                    lvItem.Tag = item;
                    lvWords.Items.Add(lvItem);
                }
            }
        }
    }
}
