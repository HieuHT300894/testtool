﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WF_TestTool.Model;
using System.IO;
using System.Xml;
using Common;
using SeleniumAPI;
using Common.Models;
using System.Reflection;
using System.ComponentModel;

namespace WF_TestTool.UC
{
    public partial class UC_Execution : UC_Base
    {
        string name = Define.Instance.Step;
        Define.eStatus iStatus = Define.eStatus.Stop;
        int delay = Define.Instance.DelayValue;
        List<CustomMethod> handles = new List<CustomMethod>();
        List<ControlData> controlDatas = new List<ControlData>();
        BindingList<ControlData> controlDatas_ListBox = new BindingList<ControlData>();
        List<Step> steps = new List<Step>();

        public UC_Execution()
        {
            InitializeComponent();
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Url,
                Text = Define.Instance.Url_Text,
                ControlType = typeof(UC_UrlHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UrlHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UrlHandle), typeof(int) }, null),
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Mouse,
                Text = Define.Instance.Mouse_Text,
                ControlType = typeof(UC_MouseHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_MouseHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_MouseHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Input,
                Text = Define.Instance.Input_Text,
                ControlType = typeof(UC_InputHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_InputHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_InputHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Key,
                Text = Define.Instance.Key_Text,
                ControlType = typeof(UC_KeyHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_KeyHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_KeyHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Display,
                Text = Define.Instance.Display_Text,
                ControlType = typeof(UC_DisplayHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_DisplayHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_DisplayHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Upload,
                Text = Define.Instance.Upload_Text,
                ControlType = typeof(UC_UploadFileHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UploadFileHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UploadFileHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Alert,
                Text = Define.Instance.Alert_Text,
                ControlType = typeof(UC_AlertHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_AlertHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_AlertHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Properties,
                Text = Define.Instance.Properties_Text,
                ControlType = typeof(UC_PropertiesHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PropertiesHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PropertiesHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Popup,
                Text = Define.Instance.Popup_Text,
                ControlType = typeof(UC_PopupHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PopupHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PopupHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.ReadFile,
                Text = Define.Instance.ReadFile_Text,
                ControlType = typeof(UC_ReadFilesHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ReadFilesHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ReadFilesHandle), typeof(int) }, null)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Scroll,
                Text = Define.Instance.Scroll_Text,
                ControlType = typeof(UC_ScrollHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ScrollHandle), typeof(XmlNode), typeof(int) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ScrollHandle), typeof(int) }, null)
            });

            lbTotal.ForeColor = Define.Instance.clrNormal;
            lbPass.ForeColor = Define.Instance.clrPass;
            lbFail.ForeColor = Define.Instance.clrFail;

            tbarDelay.Value = Define.Instance.DelayValue;

            lvFiles.Columns.Add("colName", "Name", 100);
            lvFiles.Columns.Add("colPath", "Path", 150);
            lvFiles.Format();

            lbControls.DataSource = controlDatas_ListBox;
            lbControls.FormatEx("Name", "Text");

            InitEvent();
        }
        protected override void UC_Base_Enter(object sender, EventArgs e)
        {
            tbarDelay.Value = delay;
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            LoadFiles();
        }
        private void BtnFolder_Click(object sender, EventArgs e)
        {
            LoadFolder();
        }
        private void BtnStop_Click(object sender, EventArgs e)
        {
            Abort();
        }
        private void BtnStart_Click(object sender, EventArgs e)
        {
            Start();
        }
        private void TbarDelay_ValueChanged(object sender, EventArgs e)
        {
            Define.Instance.DelayValue = tbarDelay.Value;
            delay = tbarDelay.Value;
        }
        private void lvFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            int totalSelected = lvFiles.SelectedIndices.Count;
            lbTotal.Text = $"{totalSelected }/{lvFiles.Items.Count}";
        }
        private void lbControls_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowControl();
        }
        private async void lvFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvFiles.SelectedItems.Count != 1)
                return;
            await GetControlData(lvFiles.SelectedItems[0]);
        }

        void InitEvent()
        {
            //Disable
            btnFolder.Click -= BtnFolder_Click;
            btnFile.Click -= BtnFile_Click;
            btnStart.Click -= BtnStart_Click;
            btnStop.Click -= BtnStop_Click;
            tbarDelay.ValueChanged -= TbarDelay_ValueChanged;
            lvFiles.SelectedIndexChanged -= lvFiles_SelectedIndexChanged;
            lbControls.MouseDoubleClick -= lbControls_MouseDoubleClick;
            lvFiles.MouseDoubleClick -= lvFiles_MouseDoubleClick;

            //Enable
            btnFolder.Click += BtnFolder_Click;
            btnFile.Click += BtnFile_Click;
            btnStart.Click += BtnStart_Click;
            btnStop.Click += BtnStop_Click;
            tbarDelay.ValueChanged += TbarDelay_ValueChanged;
            lvFiles.SelectedIndexChanged += lvFiles_SelectedIndexChanged;
            lbControls.MouseDoubleClick += lbControls_MouseDoubleClick;
            lvFiles.MouseDoubleClick += lvFiles_MouseDoubleClick;
        }
        void LoadFiles()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = ".xml|*.xml";
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadFiles(dialog.FileNames.ToList());
            }
        }
        void LoadFolder()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadFiles(Directory.GetFiles(dialog.SelectedPath, "*.xml").ToList());
            }
        }
        void LoadFiles(List<string> lstPaths)
        {
            controlDatas_ListBox.Clear();
            lvFiles.Items.Clear();
            pnControls.Controls.Clear();

            foreach (string path in lstPaths)
            {
                ListViewItem lvItem = new ListViewItem(new string[] { Path.GetFileNameWithoutExtension(path), path });
                lvItem.Selected = true;
                lvFiles.Items.Add(lvItem);

                //int id = 1;
                //controlDatas = new List<ControlData>();
                //if (await LoadData(path, id))
                //{
                //    ListViewItem lvItem = new ListViewItem(new string[] { Path.GetFileNameWithoutExtension(path), path });
                //    lvItem.Selected = true;
                //    lvItem.Tag = controlDatas;
                //    lvFiles.Items.Add(lvItem);
                //}
            }
        }
        async Task<bool> LoadData(string fileName, int id)
        {
            return await Task.Factory.StartNew(new Func<bool>(() =>
            {
                if (!File.Exists(fileName))
                    return false;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileName);

                XmlNode xmlStep = xmlDoc.SelectSingleNode($"/{Define.Instance.Step}");
                if (xmlStep == null)
                    return false;

                //if (xmlStep.Attributes[Define.Instance.Title] != null)
                //    uc_Comment.SetMessage(xmlStep.Attributes[Define.Instance.Title].Value, uc_Comment.GetComment());
                //if (xmlStep.Attributes[Define.Instance.Comment] != null)
                //    uc_Comment.SetMessage(uc_Comment.GetTitle(), xmlStep.Attributes[Define.Instance.Comment].Value);

                /*Load XML*/
                XmlNodeList xmlControls = xmlStep.SelectNodes($"{Define.Instance.Input}");

                foreach (XmlNode xmlControl in xmlControls)
                {
                    if (xmlControl.Attributes[Define.Instance.Type] == null)
                        continue;

                    string type = xmlControl.Attributes[Define.Instance.Type].Value;
                    CustomMethod handle = handles.FirstOrDefault(x => x.Name.ToEqualEx(type));
                    if (handle == null)
                        continue;

                    handle.ExecuteMethod(handle.AddControl, this, new object[] { null, xmlControl, id++ });
                }
                return true;
            }));
        }
        void AddControl(UC_MouseHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_MouseHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Mouse_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Mouse, Control = uc });
        }
        void AddControl(UC_InputHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_InputHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Input_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Input, Control = uc });
        }
        void AddControl(UC_KeyHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_KeyHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Key_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Key, Control = uc });
        }
        void AddControl(UC_DisplayHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_DisplayHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Display_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Display, Control = uc });
        }
        void AddControl(UC_UrlHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_UrlHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Url_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Url, Control = uc });
        }
        void AddControl(UC_UploadFileHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_UploadFileHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Upload_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Upload, Control = uc });
        }
        void AddControl(UC_AlertHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_AlertHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Alert_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Alert, Control = uc });
        }
        void AddControl(UC_PropertiesHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_PropertiesHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Properties_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Properties, Control = uc });
        }
        void AddControl(UC_PopupHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_PopupHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Popup_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Popup, Control = uc });
        }
        void AddControl(UC_ReadFilesHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_ReadFilesHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.ReadFile_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.ReadFile, Control = uc });
        }
        void AddControl(UC_ScrollHandle uc, XmlNode xmlControl, int id)
        {
            uc = new UC_ScrollHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Scroll_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Scroll, Control = uc });
        }
        void GetControl(UC_MouseHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_InputHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_KeyHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_DisplayHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_UrlHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_UploadFileHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_AlertHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_PropertiesHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_PopupHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_ReadFilesHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_ScrollHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void AddControlData(ControlData cd)
        {
            controlDatas.Add(cd);
        }
        async Task GetControlData(ListViewItem lvItem)
        {
            pnControls.Controls.Clear();
            controlDatas_ListBox.Clear();
            controlDatas = new List<ControlData>();
            if (lvItem.Tag == null)
            {
                bool bResult = await LoadData(lvItem.SubItems[1].Text, 1);
                if (bResult)
                {
                    lvItem.Tag = controlDatas;
                }
            }
            else
            {
                controlDatas = lvItem.Tag as List<ControlData>;
            }

            controlDatas.ForEach(x =>
            {
                controlDatas_ListBox.Add(x);
            });
        }
        void EditControl(Control control)
        {
            if (!pnControls.Controls.Contains(control))
                pnControls.Controls.Add(control);
            control.BringToFront();
        }
        void ShowControl()
        {
            ControlData cd = lbControls.SelectedItem as ControlData;
            if (cd == null)
                return;

            EditControl(cd.Control);
        }
        async void Start()
        {
            List<ListViewItem> selectedItems = lvFiles.GetSelectedItems();
            int totalSelected = selectedItems.Count;
            int totalItem = lvFiles.Items.Count;
            int iPassed = 0;
            int iFailed = 0;

            lbTotal.Text = $"{totalSelected}/{totalSelected}";
            lbPass.Text = $"{iPassed}";
            lbFail.Text = $"{iFailed}";

            for (int i = 0; i < totalItem; i++)
            {
                ListViewItem lvItem = lvFiles.Items[i];
                lvItem.ForeColor = Define.Instance.clrNormal;
            }

            if (totalSelected == 0)
            {
            }
            else
            {
                DisableControl();
                ChromeHandle.StartChromeDriver();

                foreach (ListViewItem item in selectedItems)
                {
                    item.Selected = false;
                    item.ForeColor = Define.Instance.clrRunning;

                    //Wait
                    await Task.Delay(delay);

                    //Close after click stop or close window
                    if (iStatus == Define.eStatus.Abort)
                    {
                        break;
                    }

                    steps.Clear();
                    await GetControlData(item);

                    int i = 0;
                    for (i = 0; i < controlDatas_ListBox.Count; i++)
                    {
                        ControlData cd = controlDatas_ListBox[i];
                        CustomMethod handle = handles.FirstOrDefault(x => x.Name.ToEqualEx(cd.Type));
                        if (handle == null)
                            continue;

                        handle.ExecuteMethod(handle.GetControl, this, new object[] { cd.Control, i });
                    }

                    //Wait
                    await Task.Delay(Define.Instance.DelayValue);

                    int j = 0;
                    for (j = 0; j < steps.Count; j++)
                    {
                        lbControls.SelectedIndex = j;

                        //Get step
                        Step step = steps[j];

                        //Wait
                        await Task.Delay(delay);

                        //Close after click stop or close window
                        if (iStatus == Define.eStatus.Abort)
                        {
                            break;
                        }

                        //Run step
                        string result = await Task.Factory.StartNew(() =>
                        {
                            return step.Func();
                        });

                        //Set log status
                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            break;
                        }
                    }

                    if (j == steps.Count)
                    {
                        item.ForeColor = Define.Instance.clrPass;
                        iPassed++;
                        lbPass.Text = $"{iPassed}";
                    }
                    else
                    {
                        item.ForeColor = Define.Instance.clrFail;
                        iFailed++;
                        lbFail.Text = $"{iFailed}";
                    }
                }

                Stop();
            }
        }
        public void Stop()
        {
            EnableControl();
        }
        public void Abort()
        {
            iStatus = Define.eStatus.Abort;
        }
        void EnableControl()
        {
            btnFile.Enabled = true;
            btnFolder.Enabled = true;
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            iStatus = Define.eStatus.Stop;

            lvFiles.SelectedIndexChanged += lvFiles_SelectedIndexChanged;
            lvFiles.MouseDoubleClick += lvFiles_MouseDoubleClick;
            lbControls.MouseDoubleClick += lbControls_MouseDoubleClick;
        }
        void DisableControl()
        {
            btnFile.Enabled = false;
            btnFolder.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            iStatus = Define.eStatus.Start;

            pnControls.Controls.Clear();

            lvFiles.SelectedIndexChanged -= lvFiles_SelectedIndexChanged;
            lvFiles.MouseDoubleClick -= lvFiles_MouseDoubleClick;
            lbControls.MouseDoubleClick -= lbControls_MouseDoubleClick;
        }
        public Define.eStatus GetStatus()
        {
            return iStatus;
        }
        public override void ClearData()
        {
            lvFiles.Items.Clear();
            pnControls.Controls.Clear();

            handles = null;
            controlDatas = null;
            controlDatas_ListBox = null;
            steps = null;

            GC.Collect();
        }
    }
}
