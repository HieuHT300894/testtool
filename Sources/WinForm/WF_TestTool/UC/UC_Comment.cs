﻿using Common;

namespace WF_TestTool.UC
{
    public partial class UC_Comment : UC_Base
    {
        public UC_Comment()
        {
            InitializeComponent();
            SetMessage();
            txtComment.Format();
        }

        public void SetMessage(string sTitle = "", string sComment = "")
        {
            txtTitle.Text = sTitle ?? string.Empty;
            txtComment.Text = sComment ?? string.Empty;
        }

        public string GetTitle()
        {
            return txtTitle.Text;
        }
        public string GetComment()
        {
            return txtComment.Text;
        }
    }
}
