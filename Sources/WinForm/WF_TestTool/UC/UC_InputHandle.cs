﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_InputHandle : UC_Base
    {
        public UC_InputHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            chkIsPosition.CheckedChanged -= ChkIsPosition_CheckedChanged;
            chkIsPosition.CheckedChanged += ChkIsPosition_CheckedChanged;
        }
        private void ChkIsPosition_CheckedChanged(object sender, EventArgs e)
        {
            numX.Enabled = chkIsPosition.Checked;
            numY.Enabled = chkIsPosition.Checked;
        }

        public void GetData(InputObject obj)
        {
            obj.sTarget = txtTarget.Text.Trim();
            obj.sValue = txtValue.Text.Trim();
            obj.bPosition = chkIsPosition.Checked;
            obj.iX = chkIsPosition.Checked ? Convert.ToInt32(numX.Value) : 0;
            obj.iY = chkIsPosition.Checked ? Convert.ToInt32(numY.Value) : 0;
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            InputObject obj = new InputObject();
            GetData(obj);

            return InputHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            txtTarget.Format();
            txtValue.Format();
            numTimeout.Format();
            numX.Format();
            numY.Format();

            string sTarget = string.Empty;
            string sValue = string.Empty;
            bool bPosition = false;
            int iX = 0;
            int iY = 0;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.Target] != null)
                    sTarget = xmlInput.Attributes[Define.Instance.Target].Value;
                if (xmlInput.Attributes[Define.Instance.Value] != null)
                    sValue = xmlInput.Attributes[Define.Instance.Value].Value;
                if (xmlInput.Attributes[Define.Instance.IsPosition] != null)
                    bool.TryParse(xmlInput.Attributes[Define.Instance.IsPosition].Value, out bPosition);
                if (xmlInput.Attributes[Define.Instance.PositionX] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.PositionX].Value, out iX);
                if (xmlInput.Attributes[Define.Instance.PositionY] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.PositionY].Value, out iY);
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            txtTarget.Text = sTarget;
            txtValue.Text = sValue;
            chkIsPosition.Checked = bPosition;
            numX.Value = iX;
            numY.Value = iY;
            numTimeout.Value = iTimeout;

            ChkIsPosition_CheckedChanged(chkIsPosition, new EventArgs());
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            InputObject obj = new InputObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Input);
            attrs.Add(Define.Instance.Target, obj.sTarget);
            attrs.Add(Define.Instance.Value, obj.sValue);
            if (obj.bPosition)
            {
                attrs.Add(Define.Instance.IsPosition, obj.bPosition);
                attrs.Add(Define.Instance.PositionX, obj.iX);
                attrs.Add(Define.Instance.PositionY, obj.iY);
            }
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}
