﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_PopupHandle : UC_Base
    {
        public UC_PopupHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            rbtnNewWindow.CheckedChanged -= RbtnNewWindow_CheckedChanged;
            rbtnBack.CheckedChanged -= Rbtn_CheckedChanged;
            rbtnNewTab.CheckedChanged -= Rbtn_CheckedChanged;
            rbtnSwitch.CheckedChanged -= Rbtn_CheckedChanged;

            rbtnNewWindow.CheckedChanged += RbtnNewWindow_CheckedChanged;
            rbtnBack.CheckedChanged += Rbtn_CheckedChanged;
            rbtnNewTab.CheckedChanged += Rbtn_CheckedChanged;
            rbtnSwitch.CheckedChanged += Rbtn_CheckedChanged;
        }

        private void Rbtn_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (rbtn.Checked)
            {
                numWidth.Enabled = false;
                numHeight.Enabled = false;
            }
        }
        private void RbtnNewWindow_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (rbtn.Checked)
            {
                numWidth.Enabled = true;
                numHeight.Enabled = true;
            }
        }

        public void GetData(PopupObject obj)
        {
            if (rbtnBack.Checked)
                obj.iType = 1;
            else if (rbtnSwitch.Checked)
                obj.iType = 2;
            else if (rbtnNewTab.Checked)
                obj.iType = 3;
            else if (rbtnNewWindow.Checked)
                obj.iType = 4;
            obj.iWidth = rbtnNewWindow.Checked ? Convert.ToInt32(numWidth.Value) : 0;
            obj.iHeight = rbtnNewWindow.Checked ? Convert.ToInt32(numHeight.Value) : 0;
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            PopupObject obj = new PopupObject();
            GetData(obj);

            return PopupHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            numTimeout.Format();
            numWidth.Format();
            numHeight.Format();

            int iType = 0;
            int iWidth = 0;
            int iHeight = 0;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.Value] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Value].Value, out iType);
                if (xmlInput.Attributes[Define.Instance.Width] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Width].Value, out iWidth);
                if (xmlInput.Attributes[Define.Instance.Height] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Height].Value, out iHeight);
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            numWidth.Value = iWidth;
            numHeight.Value = iHeight;
            numTimeout.Value = iTimeout;

            if (iType == 1)
            {
                rbtnBack.Checked = true;
                Rbtn_CheckedChanged(rbtnBack, new EventArgs());
            }
            else if (iType == 2)
            {
                rbtnSwitch.Checked = true;
                Rbtn_CheckedChanged(rbtnSwitch, new EventArgs());
            }
            else if (iType == 3)
            {
                rbtnNewTab.Checked = true;
                Rbtn_CheckedChanged(rbtnNewTab, new EventArgs());
            }
            else if (iType == 4)
            {
                rbtnNewWindow.Checked = true;
                RbtnNewWindow_CheckedChanged(rbtnNewWindow, new EventArgs());
            }
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            PopupObject obj = new PopupObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Popup);
            attrs.Add(Define.Instance.Value, obj.iType.ToString());
            if (obj.iType == 4)
            {
                attrs.Add(Define.Instance.Width, obj.iWidth.ToString());
                attrs.Add(Define.Instance.Height, obj.iHeight.ToString());
            }
            attrs.Add(Define.Instance.Timeout, obj.iTimeout.ToString());
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}
