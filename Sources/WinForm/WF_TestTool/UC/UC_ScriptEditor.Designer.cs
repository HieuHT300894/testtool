﻿namespace WF_TestTool.UC
{
    partial class UC_ScriptEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnEditor = new System.Windows.Forms.Panel();
            this.lbTag = new System.Windows.Forms.ListBox();
            this.tvTag = new WF_TestTool.UC.TreeViewEx();
            this.tpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpMain
            // 
            this.tpMain.ColumnCount = 2;
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tpMain.Controls.Add(this.pnEditor, 1, 0);
            this.tpMain.Controls.Add(this.lbTag, 0, 0);
            this.tpMain.Controls.Add(this.tvTag, 0, 1);
            this.tpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpMain.Location = new System.Drawing.Point(0, 0);
            this.tpMain.Name = "tpMain";
            this.tpMain.RowCount = 2;
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpMain.Size = new System.Drawing.Size(350, 300);
            this.tpMain.TabIndex = 0;
            // 
            // pnEditor
            // 
            this.pnEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnEditor.Location = new System.Drawing.Point(90, 3);
            this.pnEditor.Name = "pnEditor";
            this.tpMain.SetRowSpan(this.pnEditor, 2);
            this.pnEditor.Size = new System.Drawing.Size(257, 294);
            this.pnEditor.TabIndex = 0;
            // 
            // lbTag
            // 
            this.lbTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTag.FormattingEnabled = true;
            this.lbTag.Location = new System.Drawing.Point(3, 3);
            this.lbTag.Name = "lbTag";
            this.lbTag.Size = new System.Drawing.Size(81, 144);
            this.lbTag.TabIndex = 1;
            // 
            // tvTag
            // 
            this.tvTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvTag.Location = new System.Drawing.Point(3, 153);
            this.tvTag.Name = "tvTag";
            this.tvTag.Size = new System.Drawing.Size(81, 144);
            this.tvTag.TabIndex = 2;
            // 
            // UC_ScriptEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tpMain);
            this.Name = "UC_ScriptEditor";
            this.tpMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tpMain;
        private System.Windows.Forms.Panel pnEditor;
        private System.Windows.Forms.ListBox lbTag;
        private TreeViewEx tvTag;
    }
}
