﻿namespace WF_TestTool.UC
{
    partial class UC_PopupHandle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbTitle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtnNewWindow = new System.Windows.Forms.RadioButton();
            this.rbtnNewTab = new System.Windows.Forms.RadioButton();
            this.rbtnSwitch = new System.Windows.Forms.RadioButton();
            this.rbtnBack = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.numWidth = new System.Windows.Forms.NumericUpDown();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.numTimeout = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 11;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lbTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnBack, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.rbtnSwitch, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.rbtnNewTab, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.rbtnNewWindow, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.numTimeout, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(450, 125);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTitle, 11);
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Location = new System.Drawing.Point(3, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(444, 20);
            this.lbTitle.TabIndex = 0;
            this.lbTitle.Text = "label1";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "Timeout";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(23, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Size";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtnNewWindow
            // 
            this.rbtnNewWindow.AutoSize = true;
            this.rbtnNewWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnNewWindow.Location = new System.Drawing.Point(294, 23);
            this.rbtnNewWindow.Name = "rbtnNewWindow";
            this.rbtnNewWindow.Size = new System.Drawing.Size(86, 17);
            this.rbtnNewWindow.TabIndex = 5;
            this.rbtnNewWindow.TabStop = true;
            this.rbtnNewWindow.Text = "New window";
            this.rbtnNewWindow.UseVisualStyleBackColor = true;
            // 
            // rbtnNewTab
            // 
            this.rbtnNewTab.AutoSize = true;
            this.rbtnNewTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnNewTab.Location = new System.Drawing.Point(213, 23);
            this.rbtnNewTab.Name = "rbtnNewTab";
            this.rbtnNewTab.Size = new System.Drawing.Size(65, 17);
            this.rbtnNewTab.TabIndex = 4;
            this.rbtnNewTab.TabStop = true;
            this.rbtnNewTab.Text = "New tab";
            this.rbtnNewTab.UseVisualStyleBackColor = true;
            // 
            // rbtnSwitch
            // 
            this.rbtnSwitch.AutoSize = true;
            this.rbtnSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnSwitch.Location = new System.Drawing.Point(140, 23);
            this.rbtnSwitch.Name = "rbtnSwitch";
            this.rbtnSwitch.Size = new System.Drawing.Size(57, 17);
            this.rbtnSwitch.TabIndex = 3;
            this.rbtnSwitch.TabStop = true;
            this.rbtnSwitch.Text = "Switch";
            this.rbtnSwitch.UseVisualStyleBackColor = true;
            // 
            // rbtnBack
            // 
            this.rbtnBack.AutoSize = true;
            this.rbtnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnBack.Location = new System.Drawing.Point(74, 23);
            this.rbtnBack.Name = "rbtnBack";
            this.rbtnBack.Size = new System.Drawing.Size(50, 17);
            this.rbtnBack.TabIndex = 2;
            this.rbtnBack.TabStop = true;
            this.rbtnBack.Text = "Back";
            this.rbtnBack.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 8);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.numWidth, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.numHeight, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(71, 43);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(359, 26);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // numWidth
            // 
            this.numWidth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numWidth.Location = new System.Drawing.Point(3, 3);
            this.numWidth.Name = "numWidth";
            this.numWidth.Size = new System.Drawing.Size(173, 20);
            this.numWidth.TabIndex = 0;
            // 
            // numHeight
            // 
            this.numHeight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numHeight.Location = new System.Drawing.Point(182, 3);
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(174, 20);
            this.numHeight.TabIndex = 1;
            // 
            // numTimeout
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.numTimeout, 8);
            this.numTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numTimeout.Location = new System.Drawing.Point(74, 72);
            this.numTimeout.Name = "numTimeout";
            this.numTimeout.Size = new System.Drawing.Size(353, 20);
            this.numTimeout.TabIndex = 8;
            // 
            // UC_PopupHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UC_PopupHandle";
            this.Size = new System.Drawing.Size(450, 125);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.RadioButton rbtnBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtnSwitch;
        private System.Windows.Forms.RadioButton rbtnNewTab;
        private System.Windows.Forms.RadioButton rbtnNewWindow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.NumericUpDown numWidth;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.NumericUpDown numTimeout;
    }
}
