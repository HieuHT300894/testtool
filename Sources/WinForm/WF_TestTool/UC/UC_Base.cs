﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_Base : UserControl
    {
        public UC_Base()
        {
            InitializeComponent();

            Load -= UC_Base_Load;
            Load += UC_Base_Load;
            SizeChanged -= UC_Base_SizeChanged;
            SizeChanged += UC_Base_SizeChanged;
            Enter -= UC_Base_Enter;
            Enter += UC_Base_Enter;
        }

        protected virtual void UC_Base_Enter(object sender, EventArgs e)
        {
        }
        protected virtual void UC_Base_SizeChanged(object sender, EventArgs e)
        {
        }
        protected virtual void UC_Base_Load(object sender, EventArgs e)
        {
        }

        public virtual Func<string> GetFunc()
        {
            return new Func<string>(() => { return string.Empty; });
        }
        public virtual void SetTitle(string title)
        {
        }
        protected virtual void LoadData(XmlNode node)
        {
        }
        public virtual void SaveData(XmlDocument xmlDoc, XmlNode xmlParent)
        {
        }
        public virtual void ClearData()
        {

        }
    }
}
