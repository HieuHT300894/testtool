﻿namespace WF_TestTool.UC
{
    partial class UC_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tpHeader = new System.Windows.Forms.TableLayoutPanel();
            this.tbarDelay = new System.Windows.Forms.TrackBar();
            this.splitData = new System.Windows.Forms.SplitContainer();
            this.tpLeft = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cbbCommand = new System.Windows.Forms.ComboBox();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.lvControls = new System.Windows.Forms.ListView();
            this.tbRight = new System.Windows.Forms.TableLayoutPanel();
            this.pnControl = new System.Windows.Forms.Panel();
            this.uc_Comment = new WF_TestTool.UC.UC_Comment();
            this.cMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cMenu_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tpMain.SuspendLayout();
            this.tpHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbarDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitData)).BeginInit();
            this.splitData.Panel1.SuspendLayout();
            this.splitData.Panel2.SuspendLayout();
            this.splitData.SuspendLayout();
            this.tpLeft.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tbRight.SuspendLayout();
            this.cMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpMain
            // 
            this.tpMain.BackColor = System.Drawing.SystemColors.Control;
            this.tpMain.ColumnCount = 1;
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpMain.Controls.Add(this.tpHeader, 0, 0);
            this.tpMain.Controls.Add(this.splitData, 0, 1);
            this.tpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpMain.Location = new System.Drawing.Point(0, 0);
            this.tpMain.Name = "tpMain";
            this.tpMain.RowCount = 2;
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.Size = new System.Drawing.Size(900, 450);
            this.tpMain.TabIndex = 0;
            // 
            // tpHeader
            // 
            this.tpHeader.ColumnCount = 6;
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpHeader.Controls.Add(this.btnStop, 4, 0);
            this.tpHeader.Controls.Add(this.btnRun, 5, 0);
            this.tpHeader.Controls.Add(this.btnNew, 0, 0);
            this.tpHeader.Controls.Add(this.btnSave, 2, 0);
            this.tpHeader.Controls.Add(this.btnLoad, 1, 0);
            this.tpHeader.Controls.Add(this.tbarDelay, 3, 0);
            this.tpHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpHeader.Location = new System.Drawing.Point(3, 3);
            this.tpHeader.Name = "tpHeader";
            this.tpHeader.RowCount = 1;
            this.tpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpHeader.Size = new System.Drawing.Size(894, 31);
            this.tpHeader.TabIndex = 0;
            // 
            // tbarDelay
            // 
            this.tbarDelay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbarDelay.LargeChange = 250;
            this.tbarDelay.Location = new System.Drawing.Point(321, 3);
            this.tbarDelay.Maximum = 5000;
            this.tbarDelay.Minimum = 10;
            this.tbarDelay.Name = "tbarDelay";
            this.tbarDelay.Size = new System.Drawing.Size(358, 45);
            this.tbarDelay.SmallChange = 100;
            this.tbarDelay.TabIndex = 6;
            this.tbarDelay.Value = 50;
            // 
            // splitData
            // 
            this.splitData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitData.Location = new System.Drawing.Point(3, 40);
            this.splitData.Name = "splitData";
            // 
            // splitData.Panel1
            // 
            this.splitData.Panel1.Controls.Add(this.tpLeft);
            // 
            // splitData.Panel2
            // 
            this.splitData.Panel2.Controls.Add(this.tbRight);
            this.splitData.Size = new System.Drawing.Size(894, 407);
            this.splitData.SplitterDistance = 203;
            this.splitData.TabIndex = 3;
            // 
            // tpLeft
            // 
            this.tpLeft.ColumnCount = 1;
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tpLeft.Controls.Add(this.lvControls, 0, 1);
            this.tpLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpLeft.Location = new System.Drawing.Point(0, 0);
            this.tpLeft.Name = "tpLeft";
            this.tpLeft.RowCount = 2;
            this.tpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpLeft.Size = new System.Drawing.Size(203, 407);
            this.tpLeft.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.cbbCommand, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAdd, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnDown, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnUp, 3, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 31);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // cbbCommand
            // 
            this.cbbCommand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCommand.FormattingEnabled = true;
            this.cbbCommand.Location = new System.Drawing.Point(3, 5);
            this.cbbCommand.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.cbbCommand.Name = "cbbCommand";
            this.cbbCommand.Size = new System.Drawing.Size(95, 21);
            this.cbbCommand.TabIndex = 0;
            // 
            // btnDown
            // 
            this.btnDown.BackgroundImage = global::WF_TestTool.Properties.Resources.Down;
            this.btnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnDown.Location = new System.Drawing.Point(135, 3);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(25, 25);
            this.btnDown.TabIndex = 0;
            this.btnDown.UseVisualStyleBackColor = true;
            // 
            // btnUp
            // 
            this.btnUp.BackgroundImage = global::WF_TestTool.Properties.Resources.Up;
            this.btnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnUp.Location = new System.Drawing.Point(166, 3);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(25, 25);
            this.btnUp.TabIndex = 1;
            this.btnUp.UseVisualStyleBackColor = true;
            // 
            // lvControls
            // 
            this.lvControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvControls.Location = new System.Drawing.Point(3, 40);
            this.lvControls.Name = "lvControls";
            this.lvControls.Size = new System.Drawing.Size(197, 364);
            this.lvControls.TabIndex = 2;
            this.lvControls.UseCompatibleStateImageBehavior = false;
            // 
            // tbRight
            // 
            this.tbRight.BackColor = System.Drawing.SystemColors.Control;
            this.tbRight.ColumnCount = 1;
            this.tbRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbRight.Controls.Add(this.pnControl, 0, 1);
            this.tbRight.Controls.Add(this.uc_Comment, 0, 0);
            this.tbRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRight.Location = new System.Drawing.Point(0, 0);
            this.tbRight.Name = "tbRight";
            this.tbRight.RowCount = 2;
            this.tbRight.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tbRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbRight.Size = new System.Drawing.Size(687, 407);
            this.tbRight.TabIndex = 1;
            // 
            // pnControl
            // 
            this.pnControl.BackColor = System.Drawing.SystemColors.Control;
            this.pnControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnControl.Location = new System.Drawing.Point(3, 109);
            this.pnControl.Name = "pnControl";
            this.pnControl.Size = new System.Drawing.Size(681, 295);
            this.pnControl.TabIndex = 0;
            // 
            // uc_Comment
            // 
            this.uc_Comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uc_Comment.Location = new System.Drawing.Point(3, 3);
            this.uc_Comment.Name = "uc_Comment";
            this.uc_Comment.Size = new System.Drawing.Size(681, 100);
            this.uc_Comment.TabIndex = 1;
            // 
            // cMenu
            // 
            this.cMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cMenu_Delete});
            this.cMenu.Name = "cMenu";
            this.cMenu.Size = new System.Drawing.Size(108, 26);
            // 
            // cMenu_Delete
            // 
            this.cMenu_Delete.Name = "cMenu_Delete";
            this.cMenu_Delete.Size = new System.Drawing.Size(107, 22);
            this.cMenu_Delete.Text = "Delete";
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Image = global::WF_TestTool.Properties.Resources.Stop_16x16;
            this.btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStop.Location = new System.Drawing.Point(685, 3);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 25);
            this.btnStop.TabIndex = 5;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnRun
            // 
            this.btnRun.Image = global::WF_TestTool.Properties.Resources.Play_16x16;
            this.btnRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRun.Location = new System.Drawing.Point(791, 3);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(100, 25);
            this.btnRun.TabIndex = 5;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            // 
            // btnNew
            // 
            this.btnNew.Image = global::WF_TestTool.Properties.Resources.New_16x16;
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.Location = new System.Drawing.Point(3, 3);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(100, 25);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Image = global::WF_TestTool.Properties.Resources.Save_16x16;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(215, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 25);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Image = global::WF_TestTool.Properties.Resources.OpenDoc_16x16;
            this.btnLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoad.Location = new System.Drawing.Point(109, 3);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(100, 25);
            this.btnLoad.TabIndex = 3;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = global::WF_TestTool.Properties.Resources.plus;
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.Location = new System.Drawing.Point(104, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(25, 25);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // UC_Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tpMain);
            this.Name = "UC_Editor";
            this.Size = new System.Drawing.Size(900, 450);
            this.tpMain.ResumeLayout(false);
            this.tpHeader.ResumeLayout(false);
            this.tpHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbarDelay)).EndInit();
            this.splitData.Panel1.ResumeLayout(false);
            this.splitData.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitData)).EndInit();
            this.splitData.ResumeLayout(false);
            this.tpLeft.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tbRight.ResumeLayout(false);
            this.cMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tpMain;
        private System.Windows.Forms.TableLayoutPanel tpHeader;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ContextMenuStrip cMenu;
        private System.Windows.Forms.ToolStripMenuItem cMenu_Delete;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TrackBar tbarDelay;
        private System.Windows.Forms.SplitContainer splitData;
        private System.Windows.Forms.TableLayoutPanel tpLeft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox cbbCommand;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.ListView lvControls;
        private System.Windows.Forms.TableLayoutPanel tbRight;
        private System.Windows.Forms.Panel pnControl;
        private UC_Comment uc_Comment;
    }
}