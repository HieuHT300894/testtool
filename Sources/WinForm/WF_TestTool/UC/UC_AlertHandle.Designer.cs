﻿namespace WF_TestTool.UC
{
    partial class UC_AlertHandle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbInput = new System.Windows.Forms.Label();
            this.rbtnCancel = new System.Windows.Forms.RadioButton();
            this.rbtnOk = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.numTimeout = new System.Windows.Forms.NumericUpDown();
            this.txtText = new System.Windows.Forms.TextBox();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.lbTitle = new System.Windows.Forms.Label();
            this.rbtnSimple = new System.Windows.Forms.RadioButton();
            this.rbtnPrompt = new System.Windows.Forms.RadioButton();
            this.rbtnConfirmation = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Text";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbInput
            // 
            this.lbInput.AutoSize = true;
            this.lbInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbInput.Location = new System.Drawing.Point(23, 69);
            this.lbInput.Name = "lbInput";
            this.lbInput.Size = new System.Drawing.Size(45, 26);
            this.lbInput.TabIndex = 2;
            this.lbInput.Text = "Input";
            this.lbInput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtnCancel
            // 
            this.rbtnCancel.AutoSize = true;
            this.rbtnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnCancel.Location = new System.Drawing.Point(369, 124);
            this.rbtnCancel.Name = "rbtnCancel";
            this.rbtnCancel.Size = new System.Drawing.Size(58, 17);
            this.rbtnCancel.TabIndex = 1;
            this.rbtnCancel.TabStop = true;
            this.rbtnCancel.Text = "Cancel";
            this.rbtnCancel.UseVisualStyleBackColor = true;
            // 
            // rbtnOk
            // 
            this.rbtnOk.AutoSize = true;
            this.rbtnOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnOk.Location = new System.Drawing.Point(323, 124);
            this.rbtnOk.Name = "rbtnOk";
            this.rbtnOk.Size = new System.Drawing.Size(40, 17);
            this.rbtnOk.TabIndex = 0;
            this.rbtnOk.TabStop = true;
            this.rbtnOk.Text = "OK";
            this.rbtnOk.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 5;
            this.label3.Text = "Timeout";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimeout
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.numTimeout, 8);
            this.numTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numTimeout.Location = new System.Drawing.Point(74, 98);
            this.numTimeout.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numTimeout.Name = "numTimeout";
            this.numTimeout.Size = new System.Drawing.Size(353, 20);
            this.numTimeout.TabIndex = 6;
            this.numTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numTimeout.ThousandsSeparator = true;
            // 
            // txtText
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.txtText, 8);
            this.txtText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtText.Location = new System.Drawing.Point(74, 46);
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(353, 20);
            this.txtText.TabIndex = 7;
            // 
            // txtInput
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.txtInput, 8);
            this.txtInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInput.Location = new System.Drawing.Point(74, 72);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(353, 20);
            this.txtInput.TabIndex = 8;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.lbTitle, 11);
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Location = new System.Drawing.Point(3, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(444, 20);
            this.lbTitle.TabIndex = 2;
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbtnSimple
            // 
            this.rbtnSimple.AutoSize = true;
            this.rbtnSimple.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnSimple.Location = new System.Drawing.Point(74, 23);
            this.rbtnSimple.Name = "rbtnSimple";
            this.rbtnSimple.Size = new System.Drawing.Size(56, 17);
            this.rbtnSimple.TabIndex = 0;
            this.rbtnSimple.Text = "Simple";
            this.rbtnSimple.UseVisualStyleBackColor = true;
            // 
            // rbtnPrompt
            // 
            this.rbtnPrompt.AutoSize = true;
            this.rbtnPrompt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnPrompt.Location = new System.Drawing.Point(146, 23);
            this.rbtnPrompt.Name = "rbtnPrompt";
            this.rbtnPrompt.Size = new System.Drawing.Size(58, 17);
            this.rbtnPrompt.TabIndex = 1;
            this.rbtnPrompt.Text = "Prompt";
            this.rbtnPrompt.UseVisualStyleBackColor = true;
            // 
            // rbtnConfirmation
            // 
            this.rbtnConfirmation.AutoSize = true;
            this.rbtnConfirmation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnConfirmation.Location = new System.Drawing.Point(220, 23);
            this.rbtnConfirmation.Name = "rbtnConfirmation";
            this.rbtnConfirmation.Size = new System.Drawing.Size(83, 17);
            this.rbtnConfirmation.TabIndex = 2;
            this.rbtnConfirmation.Text = "Confirmation";
            this.rbtnConfirmation.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 11;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.lbInput, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.numTimeout, 2, 4);
            this.tableLayoutPanel10.Controls.Add(this.label3, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.txtInput, 2, 3);
            this.tableLayoutPanel10.Controls.Add(this.txtText, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.label1, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.rbtnConfirmation, 6, 1);
            this.tableLayoutPanel10.Controls.Add(this.rbtnSimple, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.lbTitle, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.rbtnPrompt, 4, 1);
            this.tableLayoutPanel10.Controls.Add(this.rbtnCancel, 9, 5);
            this.tableLayoutPanel10.Controls.Add(this.rbtnOk, 8, 5);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 7;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(450, 170);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // UC_AlertHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel10);
            this.Name = "UC_AlertHandle";
            this.Size = new System.Drawing.Size(450, 170);
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RadioButton rbtnSimple;
        private System.Windows.Forms.RadioButton rbtnPrompt;
        private System.Windows.Forms.RadioButton rbtnConfirmation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbInput;
        private System.Windows.Forms.RadioButton rbtnOk;
        private System.Windows.Forms.RadioButton rbtnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numTimeout;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
    }
}
