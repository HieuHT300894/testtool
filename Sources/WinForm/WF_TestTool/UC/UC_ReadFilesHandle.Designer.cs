﻿namespace WF_TestTool.UC
{
    partial class UC_ReadFilesHandle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tpMain = new System.Windows.Forms.TableLayoutPanel();
            this.txtRegex = new System.Windows.Forms.TextBox();
            this.numTimeout = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.rbtnFirst = new System.Windows.Forms.RadioButton();
            this.rbtnLast = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnCustom = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numStart = new System.Windows.Forms.NumericUpDown();
            this.numEnd = new System.Windows.Forms.NumericUpDown();
            this.lvWords = new System.Windows.Forms.ListView();
            this.cMenu_Word = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cMenu_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.tpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).BeginInit();
            this.cMenu_Word.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpMain
            // 
            this.tpMain.ColumnCount = 11;
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.Controls.Add(this.txtRegex, 2, 4);
            this.tpMain.Controls.Add(this.numTimeout, 2, 7);
            this.tpMain.Controls.Add(this.label4, 1, 7);
            this.tpMain.Controls.Add(this.txtPath, 2, 3);
            this.tpMain.Controls.Add(this.btnLoad, 9, 3);
            this.tpMain.Controls.Add(this.label2, 1, 4);
            this.tpMain.Controls.Add(this.txtValue, 2, 5);
            this.tpMain.Controls.Add(this.label3, 1, 5);
            this.tpMain.Controls.Add(this.lbTitle, 0, 0);
            this.tpMain.Controls.Add(this.btnEdit, 8, 3);
            this.tpMain.Controls.Add(this.rbtnFirst, 2, 1);
            this.tpMain.Controls.Add(this.rbtnLast, 4, 1);
            this.tpMain.Controls.Add(this.label1, 1, 3);
            this.tpMain.Controls.Add(this.rbtnCustom, 6, 1);
            this.tpMain.Controls.Add(this.tableLayoutPanel1, 2, 2);
            this.tpMain.Controls.Add(this.lvWords, 2, 6);
            this.tpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpMain.Location = new System.Drawing.Point(0, 0);
            this.tpMain.Name = "tpMain";
            this.tpMain.RowCount = 9;
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.Size = new System.Drawing.Size(450, 300);
            this.tpMain.TabIndex = 0;
            // 
            // txtRegex
            // 
            this.tpMain.SetColumnSpan(this.txtRegex, 8);
            this.txtRegex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRegex.Location = new System.Drawing.Point(74, 103);
            this.txtRegex.Name = "txtRegex";
            this.txtRegex.Size = new System.Drawing.Size(353, 20);
            this.txtRegex.TabIndex = 10;
            // 
            // numTimeout
            // 
            this.tpMain.SetColumnSpan(this.numTimeout, 8);
            this.numTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numTimeout.Location = new System.Drawing.Point(74, 257);
            this.numTimeout.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numTimeout.Name = "numTimeout";
            this.numTimeout.Size = new System.Drawing.Size(353, 20);
            this.numTimeout.TabIndex = 5;
            this.numTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numTimeout.ThousandsSeparator = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(23, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Timeout";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPath
            // 
            this.tpMain.SetColumnSpan(this.txtPath, 6);
            this.txtPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPath.Location = new System.Drawing.Point(74, 72);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(241, 20);
            this.txtPath.TabIndex = 11;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(377, 72);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(50, 25);
            this.btnLoad.TabIndex = 12;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(23, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Regex";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtValue
            // 
            this.tpMain.SetColumnSpan(this.txtValue, 8);
            this.txtValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValue.Location = new System.Drawing.Point(74, 129);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(353, 20);
            this.txtValue.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 1;
            this.label3.Text = "Value";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.tpMain.SetColumnSpan(this.lbTitle, 11);
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Location = new System.Drawing.Point(3, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(444, 20);
            this.lbTitle.TabIndex = 9;
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(321, 72);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(50, 25);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // rbtnFirst
            // 
            this.rbtnFirst.AutoSize = true;
            this.rbtnFirst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnFirst.Location = new System.Drawing.Point(74, 23);
            this.rbtnFirst.Name = "rbtnFirst";
            this.rbtnFirst.Size = new System.Drawing.Size(44, 17);
            this.rbtnFirst.TabIndex = 11;
            this.rbtnFirst.TabStop = true;
            this.rbtnFirst.Text = "First";
            this.rbtnFirst.UseVisualStyleBackColor = true;
            // 
            // rbtnLast
            // 
            this.rbtnLast.AutoSize = true;
            this.rbtnLast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnLast.Location = new System.Drawing.Point(134, 23);
            this.rbtnLast.Name = "rbtnLast";
            this.rbtnLast.Size = new System.Drawing.Size(45, 17);
            this.rbtnLast.TabIndex = 11;
            this.rbtnLast.TabStop = true;
            this.rbtnLast.Text = "Last";
            this.rbtnLast.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(23, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbtnCustom
            // 
            this.rbtnCustom.AutoSize = true;
            this.rbtnCustom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnCustom.Location = new System.Drawing.Point(195, 23);
            this.rbtnCustom.Name = "rbtnCustom";
            this.rbtnCustom.Size = new System.Drawing.Size(60, 17);
            this.rbtnCustom.TabIndex = 11;
            this.rbtnCustom.TabStop = true;
            this.rbtnCustom.Text = "Custom";
            this.rbtnCustom.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tpMain.SetColumnSpan(this.tableLayoutPanel1, 8);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.numStart, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.numEnd, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(71, 43);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(359, 26);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 26);
            this.label6.TabIndex = 0;
            this.label6.Text = "Start";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(184, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 26);
            this.label7.TabIndex = 1;
            this.label7.Text = "End";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numStart
            // 
            this.numStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numStart.Location = new System.Drawing.Point(38, 3);
            this.numStart.Name = "numStart";
            this.numStart.Size = new System.Drawing.Size(140, 20);
            this.numStart.TabIndex = 2;
            // 
            // numEnd
            // 
            this.numEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numEnd.Location = new System.Drawing.Point(216, 3);
            this.numEnd.Name = "numEnd";
            this.numEnd.Size = new System.Drawing.Size(140, 20);
            this.numEnd.TabIndex = 3;
            // 
            // lvWords
            // 
            this.tpMain.SetColumnSpan(this.lvWords, 8);
            this.lvWords.ContextMenuStrip = this.cMenu_Word;
            this.lvWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvWords.Location = new System.Drawing.Point(74, 155);
            this.lvWords.Name = "lvWords";
            this.lvWords.Size = new System.Drawing.Size(353, 96);
            this.lvWords.TabIndex = 14;
            this.lvWords.UseCompatibleStateImageBehavior = false;
            // 
            // cMenu_Word
            // 
            this.cMenu_Word.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cMenu_Delete});
            this.cMenu_Word.Name = "cMenu_Word";
            this.cMenu_Word.Size = new System.Drawing.Size(108, 26);
            // 
            // cMenu_Delete
            // 
            this.cMenu_Delete.Name = "cMenu_Delete";
            this.cMenu_Delete.Size = new System.Drawing.Size(107, 22);
            this.cMenu_Delete.Text = "Delete";
            // 
            // UC_ReadFilesHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tpMain);
            this.Name = "UC_ReadFilesHandle";
            this.Size = new System.Drawing.Size(450, 300);
            this.tpMain.ResumeLayout(false);
            this.tpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).EndInit();
            this.cMenu_Word.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tpMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numTimeout;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.TextBox txtRegex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.RadioButton rbtnFirst;
        private System.Windows.Forms.RadioButton rbtnLast;
        private System.Windows.Forms.RadioButton rbtnCustom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numStart;
        private System.Windows.Forms.NumericUpDown numEnd;
        private System.Windows.Forms.ListView lvWords;
        private System.Windows.Forms.ContextMenuStrip cMenu_Word;
        private System.Windows.Forms.ToolStripMenuItem cMenu_Delete;
    }
}