﻿namespace WF_TestTool.UC
{
    partial class UC_KeyHandle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tpMain = new System.Windows.Forms.TableLayoutPanel();
            this.lbTitle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numTimeout = new System.Windows.Forms.NumericUpDown();
            this.chkIsPosition = new System.Windows.Forms.CheckBox();
            this.numX = new System.Windows.Forms.NumericUpDown();
            this.numY = new System.Windows.Forms.NumericUpDown();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.cbbKey = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnIsKeyPress = new System.Windows.Forms.RadioButton();
            this.rbtnIsKeyUp = new System.Windows.Forms.RadioButton();
            this.rbtnIsKeyDown = new System.Windows.Forms.RadioButton();
            this.tpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpMain
            // 
            this.tpMain.ColumnCount = 6;
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.Controls.Add(this.lbTitle, 0, 0);
            this.tpMain.Controls.Add(this.label4, 1, 5);
            this.tpMain.Controls.Add(this.label3, 1, 4);
            this.tpMain.Controls.Add(this.label2, 1, 3);
            this.tpMain.Controls.Add(this.label6, 1, 2);
            this.tpMain.Controls.Add(this.numTimeout, 2, 5);
            this.tpMain.Controls.Add(this.chkIsPosition, 2, 4);
            this.tpMain.Controls.Add(this.numX, 3, 4);
            this.tpMain.Controls.Add(this.numY, 4, 4);
            this.tpMain.Controls.Add(this.txtTarget, 2, 3);
            this.tpMain.Controls.Add(this.cbbKey, 2, 2);
            this.tpMain.Controls.Add(this.tableLayoutPanel1, 2, 1);
            this.tpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpMain.Location = new System.Drawing.Point(0, 0);
            this.tpMain.Name = "tpMain";
            this.tpMain.RowCount = 7;
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tpMain.Size = new System.Drawing.Size(450, 180);
            this.tpMain.TabIndex = 0;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.tpMain.SetColumnSpan(this.lbTitle, 6);
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Location = new System.Drawing.Point(3, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(444, 20);
            this.lbTitle.TabIndex = 14;
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(23, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Timeout";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Position";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(23, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Target";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(23, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 27);
            this.label6.TabIndex = 11;
            this.label6.Text = "Key";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimeout
            // 
            this.tpMain.SetColumnSpan(this.numTimeout, 3);
            this.numTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numTimeout.Location = new System.Drawing.Point(74, 131);
            this.numTimeout.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numTimeout.Name = "numTimeout";
            this.numTimeout.Size = new System.Drawing.Size(353, 20);
            this.numTimeout.TabIndex = 8;
            this.numTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numTimeout.ThousandsSeparator = true;
            // 
            // chkIsPosition
            // 
            this.chkIsPosition.AutoSize = true;
            this.chkIsPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkIsPosition.Location = new System.Drawing.Point(74, 105);
            this.chkIsPosition.Name = "chkIsPosition";
            this.chkIsPosition.Size = new System.Drawing.Size(15, 20);
            this.chkIsPosition.TabIndex = 5;
            this.chkIsPosition.UseVisualStyleBackColor = true;
            // 
            // numX
            // 
            this.numX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numX.Location = new System.Drawing.Point(95, 105);
            this.numX.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numX.Minimum = new decimal(new int[] {
            -2147483648,
            0,
            0,
            -2147483648});
            this.numX.Name = "numX";
            this.numX.Size = new System.Drawing.Size(163, 20);
            this.numX.TabIndex = 6;
            this.numX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numX.ThousandsSeparator = true;
            // 
            // numY
            // 
            this.numY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numY.Location = new System.Drawing.Point(264, 105);
            this.numY.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numY.Minimum = new decimal(new int[] {
            -2147483648,
            0,
            0,
            -2147483648});
            this.numY.Name = "numY";
            this.numY.Size = new System.Drawing.Size(163, 20);
            this.numY.TabIndex = 7;
            this.numY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numY.ThousandsSeparator = true;
            // 
            // txtTarget
            // 
            this.tpMain.SetColumnSpan(this.txtTarget, 3);
            this.txtTarget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTarget.Location = new System.Drawing.Point(74, 79);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(353, 20);
            this.txtTarget.TabIndex = 15;
            // 
            // cbbKey
            // 
            this.tpMain.SetColumnSpan(this.cbbKey, 3);
            this.cbbKey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbbKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbKey.FormattingEnabled = true;
            this.cbbKey.Location = new System.Drawing.Point(74, 52);
            this.cbbKey.Name = "cbbKey";
            this.cbbKey.Size = new System.Drawing.Size(353, 21);
            this.cbbKey.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tpMain.SetColumnSpan(this.tableLayoutPanel1, 3);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.rbtnIsKeyPress, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnIsKeyUp, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnIsKeyDown, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(74, 23);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(353, 23);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // rbtnIsKeyPress
            // 
            this.rbtnIsKeyPress.AutoSize = true;
            this.rbtnIsKeyPress.Location = new System.Drawing.Point(3, 3);
            this.rbtnIsKeyPress.Name = "rbtnIsKeyPress";
            this.rbtnIsKeyPress.Size = new System.Drawing.Size(71, 17);
            this.rbtnIsKeyPress.TabIndex = 0;
            this.rbtnIsKeyPress.TabStop = true;
            this.rbtnIsKeyPress.Text = "Key press";
            this.rbtnIsKeyPress.UseVisualStyleBackColor = true;
            // 
            // rbtnIsKeyUp
            // 
            this.rbtnIsKeyUp.AutoSize = true;
            this.rbtnIsKeyUp.Location = new System.Drawing.Point(178, 3);
            this.rbtnIsKeyUp.Name = "rbtnIsKeyUp";
            this.rbtnIsKeyUp.Size = new System.Drawing.Size(58, 17);
            this.rbtnIsKeyUp.TabIndex = 2;
            this.rbtnIsKeyUp.TabStop = true;
            this.rbtnIsKeyUp.Text = "Key up";
            this.rbtnIsKeyUp.UseVisualStyleBackColor = true;
            // 
            // rbtnIsKeyDown
            // 
            this.rbtnIsKeyDown.AutoSize = true;
            this.rbtnIsKeyDown.Location = new System.Drawing.Point(90, 3);
            this.rbtnIsKeyDown.Name = "rbtnIsKeyDown";
            this.rbtnIsKeyDown.Size = new System.Drawing.Size(72, 17);
            this.rbtnIsKeyDown.TabIndex = 1;
            this.rbtnIsKeyDown.TabStop = true;
            this.rbtnIsKeyDown.Text = "Key down";
            this.rbtnIsKeyDown.UseVisualStyleBackColor = true;
            // 
            // UC_KeyHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tpMain);
            this.Name = "UC_KeyHandle";
            this.Size = new System.Drawing.Size(450, 180);
            this.tpMain.ResumeLayout(false);
            this.tpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tpMain;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkIsPosition;
        private System.Windows.Forms.NumericUpDown numX;
        private System.Windows.Forms.NumericUpDown numY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numTimeout;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbKey;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton rbtnIsKeyPress;
        private System.Windows.Forms.RadioButton rbtnIsKeyDown;
        private System.Windows.Forms.RadioButton rbtnIsKeyUp;
    }
}