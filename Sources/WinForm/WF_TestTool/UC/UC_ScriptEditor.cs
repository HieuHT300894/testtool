﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Collections.Generic;
using WF_TestTool.Model;
using System.Linq;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using XmlIntelliSense.App.XHelper;
using System.Text;
using System.ComponentModel;
using System.Data;
using ICSharpCode.AvalonEdit.Document;
using System.Windows;
using Common;
using System.Reflection;

namespace WF_TestTool.UC
{
    public partial class UC_ScriptEditor : UC_Base
    {
        TextEditor editor;
        ElementHost host;
        FoldingManager foldingManager;
        XmlFoldingStrategy foldingStrategy;
        CompletionWindow completionWindow;
        XmlSchemaSet schemas;
        List<XsdElementInformation> XsdInformation;
        BindingList<Tag> tags;
        Timer timer;

        public UC_ScriptEditor()
        {
            InitializeComponent();

            editor = new TextEditor();
            host = new ElementHost();
            foldingManager = FoldingManager.Install(editor.TextArea);
            foldingStrategy = new XmlFoldingStrategy();
            schemas = new XmlSchemaSet();
            tags = new BindingList<Tag>();
            timer = new Timer() { Interval = 1000 };
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            OpenEditor();
            InitEvent();
        }
        protected override void UC_Base_SizeChanged(object sender, EventArgs e)
        {
            editor.Options.HighlightCurrentLine = false;
            editor.Options.HighlightCurrentLine = true;
        }

        private void Editor_TextChanged(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Start();
        }
        private void TextArea_TextEntered(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            try
            {
                switch (e.Text)
                {
                    case ">":
                        {
                            //auto-insert closing element
                            int offset = editor.CaretOffset;
                            string s = XmlIntelliSense.App.XHelper.XmlParser.GetElementAtCursor(editor.Text, offset - 1);
                            if (!string.IsNullOrWhiteSpace(s) && "!--" != s)
                            {
                                if (!XmlIntelliSense.App.XHelper.XmlParser.IsClosingElement(editor.Text, offset - 1, s))
                                {
                                    string endElement = "</" + s + ">";
                                    var rightOfCursor = editor.Text.Substring(offset, Math.Max(0, Math.Min(endElement.Length + 50, editor.Text.Length) - offset - 1)).TrimStart();
                                    if (!rightOfCursor.StartsWith(endElement))
                                    {
                                        editor.TextArea.Document.Insert(offset, endElement);
                                        editor.CaretOffset = offset;
                                    }
                                }
                            }
                            break;
                        }
                    case "/":
                        {
                            int offset = editor.CaretOffset;
                            if (editor.Text.Length > offset + 2 && editor.Text[offset] == '>')
                            {
                                //remove closing tag if exist
                                string s = XmlIntelliSense.App.XHelper.XmlParser.GetElementAtCursor(editor.Text, offset - 1);
                                if (!string.IsNullOrWhiteSpace(s))
                                {
                                    //search closing end tag. Element must be empty (whitespace allowed)  
                                    //"<hallo>  </hallo>" --> enter '/' --> "<hallo/>  "
                                    string expectedEndTag = "</" + s + ">";
                                    for (int i = offset + 1; i < editor.Text.Length - expectedEndTag.Length + 1; i++)
                                    {
                                        if (!char.IsWhiteSpace(editor.Text[i]))
                                        {
                                            if (editor.Text.Substring(i, expectedEndTag.Length) == expectedEndTag)
                                            {
                                                //remove already existing endTag
                                                editor.Document.Remove(i, expectedEndTag.Length);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    case "<":
                        var parentElement = XmlIntelliSense.App.XHelper.XmlParser.GetParentElementPath(editor.Text);
                        var elementAutocompleteList = ProvidePossibleElementsAutocomplete(parentElement);

                        InvokeCompletionWindow(elementAutocompleteList, false);
                        break;
                    case " ":
                        {
                            var currentElement = XmlIntelliSense.App.XHelper.XmlParser.GetActiveElementStartPath(editor.Text, editor.CaretOffset);
                            var attributeautocompletelist = ProvidePossibleAttributesAutocomplete(currentElement);
                            InvokeCompletionWindow(attributeautocompletelist, true);
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }

            if (e.Text.Length > 0)
            {
                char c = e.Text[0];
                if (!(char.IsLetterOrDigit(c) || char.IsPunctuation(c)))
                {
                    e.Handled = true;
                }
            }
        }
        private void CompletionWindow_Closed(object sender, EventArgs e)
        {
            completionWindow = null;
        }
        private void LbTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lbTag.SelectedIndexChanged -= LbTag_SelectedIndexChanged;
            //editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;

            //ClearHighlight();
            //Tag tag = lbTag.SelectedItem as Tag;
            //if (tag != null)
            //{
            //    editor.TextArea.Caret.Offset = tag.OpenOffset;
            //}
            //AddHighlight(tag);

            //lbTag.SelectedIndexChanged += LbTag_SelectedIndexChanged;
            //editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Tick -= Timer_Tick;
            lbTag.MouseClick -= LbTag_MouseClick;
            tvTag.NodeMouseClick -= TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;

            timer.Stop();
            foldingStrategy.UpdateFoldings(foldingManager, editor.Document);
            ParseStringToXMl();

            timer.Tick += Timer_Tick;
            lbTag.MouseClick += LbTag_MouseClick;
            tvTag.NodeMouseClick += TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
        }
        private void Caret_PositionChanged(object sender, EventArgs e)
        {
            lbTag.MouseClick -= LbTag_MouseClick;
            tvTag.NodeMouseClick -= TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;

            ClearHighlight();
            Tag tag = null;
            DocumentLine line = editor.Document.GetLineByOffset(editor.TextArea.Caret.Offset);
            if (line != null)
            {
                tag = tags.FirstOrDefault(x => (x.StartLine == line.LineNumber || x.EndLine == line.LineNumber) && ((x.OpenStartOffset <= editor.TextArea.Caret.Offset && x.OpenEndOffset >= editor.TextArea.Caret.Offset) || (x.CloseStartOffset <= editor.TextArea.Caret.Offset && x.CloseEndOffset >= editor.TextArea.Caret.Offset)));
                if (tag != null)
                {
                    lbTag.SelectedIndex = tag.ID;
                    TreeNode node = tvTag.Get().FirstOrDefault(x => (x.Tag as Tag).ID.Equals(tag.ID));
                    if (node != null)
                        tvTag.SelectedNode = node;
                }
            }
            AddHighlight(tag);

            lbTag.MouseClick += LbTag_MouseClick;
            tvTag.NodeMouseClick += TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
        }
        private void TvTag_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            lbTag.MouseClick -= LbTag_MouseClick;
            tvTag.NodeMouseClick -= TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;

            ClearHighlight();
            Tag tag = e.Node.Tag as Tag;
            if (tag != null)
            {
                editor.TextArea.Caret.Offset = tag.OpenOffset;
                DocumentLine line = editor.Document.GetLineByOffset(editor.TextArea.Caret.Offset);
                if (line != null)
                    lbTag.SelectedIndex = tags.ToList().FindIndex(x => (x.StartLine == line.LineNumber || x.EndLine == line.LineNumber) && ((x.OpenStartOffset <= editor.TextArea.Caret.Offset && x.OpenEndOffset >= editor.TextArea.Caret.Offset) || (x.CloseStartOffset <= editor.TextArea.Caret.Offset && x.CloseEndOffset >= editor.TextArea.Caret.Offset)));
            }
            AddHighlight(tag);

            lbTag.MouseClick += LbTag_MouseClick;
            tvTag.NodeMouseClick += TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
        }
        private void LbTag_MouseClick(object sender, MouseEventArgs e)
        {
            lbTag.MouseClick -= LbTag_MouseClick;
            tvTag.NodeMouseClick -= TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;

            ClearHighlight();
            Tag tag = lbTag.SelectedItem as Tag;
            if (tag != null)
            {
                editor.TextArea.Caret.Offset = tag.OpenOffset;
                TreeNode node = tvTag.Get().FirstOrDefault(x => (x.Tag as Tag).ID.Equals(tag.ID));
                if (node != null)
                    tvTag.SelectedNode = node;
            }
            AddHighlight(tag);

            lbTag.MouseClick += LbTag_MouseClick;
            tvTag.NodeMouseClick += TvTag_NodeMouseClick;
            editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
        }

        void OpenEditor()
        {
            try
            {
                host.Name = "ElementHost";
                host.Dock = DockStyle.Fill;
                host.Child = editor;
                pnEditor.Controls.Add(host);

                editor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("XML");
                editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
                editor.FontSize = 13.25;
                editor.FontWeight = FontWeights.Regular;
                editor.ShowLineNumbers = true;
                editor.Options.HighlightCurrentLine = true;

                foldingStrategy.UpdateFoldings(foldingManager, editor.Document);

                schemas.Add("", XmlReader.Create(new StringReader(File.ReadAllText(@"Schema1.xsd"))));
                schemas.Compile();
                XsdInformation = XsdParser.AnalyseSchema(schemas);

                lbTag.DataSource = tags;
                lbTag.FormatEx("Name", "Name");
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
        }
        void InitEvent()
        {
            timer.Tick -= Timer_Tick;
            editor.TextChanged -= Editor_TextChanged;
            editor.TextArea.TextEntered -= TextArea_TextEntered;
            editor.TextArea.Caret.PositionChanged -= Caret_PositionChanged;
            tvTag.NodeMouseClick -= TvTag_NodeMouseClick;
            lbTag.MouseClick -= LbTag_MouseClick;

            timer.Tick += Timer_Tick;
            editor.TextChanged += Editor_TextChanged;
            editor.TextArea.TextEntered += TextArea_TextEntered;
            editor.TextArea.Caret.PositionChanged += Caret_PositionChanged;
            tvTag.NodeMouseClick += TvTag_NodeMouseClick;
            lbTag.MouseClick += LbTag_MouseClick;
        }
        public void CloseEditor()
        {
            if (completionWindow != null)
                completionWindow.Close();
        }
        void InvokeCompletionWindow(List<KeyValuePair<string, string>> elementAutocompleteList, bool isAttribute)
        {
            var completionWindow = new CompletionWindow(editor.TextArea);
            IList<ICompletionData> data = completionWindow.CompletionList.CompletionData;
            if (elementAutocompleteList.Any())
            {
                foreach (var autocompleteelement in elementAutocompleteList)
                {
                    data.Add(new XmlCompletionData(autocompleteelement.Key, autocompleteelement.Value, isAttribute));
                }
                completionWindow.Show();
                completionWindow.Closed -= CompletionWindow_Closed;
                completionWindow.Closed += CompletionWindow_Closed;
            }
        }
        List<KeyValuePair<string, string>> ProvidePossibleElementsAutocomplete(XmlElementInformation path)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            if (path.IsEmpty)
            {
                var xsdResultForGivenElementPath = XsdInformation.FirstOrDefault(x => x.IsRoot);

                if (xsdResultForGivenElementPath != null)
                {
                    result.Add(new KeyValuePair<string, string>(xsdResultForGivenElementPath.Name, xsdResultForGivenElementPath.DataType));
                }
            }
            else
            {
                StringBuilder xpath = new StringBuilder();
                xpath.Append("/");
                foreach (var element in path.Elements)
                {
                    xpath.Append("/" + element.Name);
                }

                var xsdResultForGivenElementPath = XsdInformation.FirstOrDefault(x => x.XPathLikeKey.ToLowerInvariant() == xpath.ToString().ToLowerInvariant());

                if (xsdResultForGivenElementPath != null)
                {
                    foreach (var xsdInformationElement in xsdResultForGivenElementPath.Elements)
                    {
                        result.Add(new KeyValuePair<string, string>(xsdInformationElement.Name, xsdInformationElement.DataType));
                    }
                }
            }


            return result;
        }
        List<KeyValuePair<string, string>> ProvidePossibleAttributesAutocomplete(XmlElementInformation path)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

            if (path.IsEmpty)
            {
                var xsdResultForGivenElementPath = XsdInformation.FirstOrDefault(x => x.IsRoot);

                if (xsdResultForGivenElementPath != null)
                {
                    foreach (var xsdInformationAttribute in xsdResultForGivenElementPath.Attributes)
                    {
                        result.Add(new KeyValuePair<string, string>(xsdInformationAttribute.Name, xsdInformationAttribute.DataType));
                    }
                }
            }
            else
            {
                StringBuilder xpath = new StringBuilder();
                xpath.Append("/");
                foreach (var element in path.Elements)
                {
                    xpath.Append("/" + element.Name);
                }

                var xsdResultForGivenElementPath = XsdInformation.FirstOrDefault(x => x.XPathLikeKey.ToLowerInvariant() == xpath.ToString().ToLowerInvariant());

                if (xsdResultForGivenElementPath != null)
                {
                    foreach (var xsdInformationAttribute in xsdResultForGivenElementPath.Attributes)
                    {
                        result.Add(new KeyValuePair<string, string>(xsdInformationAttribute.Name, xsdInformationAttribute.DataType));
                    }
                }
            }
            return result;
        }
        void IntelliSense(object sender, EventArgs eventArgs)
        {
            //richTextBox1.ResetText();

            //XmlTag tag;
            //HtmlParser parse = new HtmlParser(editor.Text);
            //while (parse.ParseNext("*", out tag))
            //{
            //    if (tag != null)
            //    {
            //        richTextBox1.AppendText($"[{tag.Indicate}]-[{tag.Name}]{Environment.NewLine}");
            //    }
            //}

            //var GetActiveElementStartPath = XmlParser.GetActiveElementStartPath(editor.Text, editor.TextArea.Caret.Offset);
            //var GetParentElementPath = XmlParser.GetParentElementPath(editor.Text);
            //var GetElementAtCursor = XmlParser.GetElementAtCursor(editor.Text, editor.TextArea.Caret.Offset);

            //StringBuilder builder = new StringBuilder();
            //builder.AppendLine("GetActiveElementStartPath: " + GetActiveElementStartPath.ToString());
            //builder.AppendLine("GetParentElementPath: " + GetParentElementPath.ToString());
            //builder.AppendLine("GetElementAtCursor: " + GetElementAtCursor.ToString());
            //this.CurrentPath.Text = builder.ToString();
        }
        void ParseStringToXMl()
        {
            int id = 0;
            List<Tag> temp = new List<Tag>();
            ClearHighlight();

            Tag tag;
            Model.XmlParser parse = new Model.XmlParser(editor.Text);
            while (parse.ParseNext("*", out tag))
            {
                if (tag != null)
                {
                    tag.ID = id++;
                    temp.Add(tag);

                    DocumentLine lineStart = editor.Document.GetLineByOffset(tag.StartOffset);
                    tag.StartLine = lineStart.LineNumber;

                    FoldingSection fold = foldingManager.AllFoldings.FirstOrDefault(x => x.StartOffset == tag.StartOffset);
                    if (fold != null)
                    {
                        tag.EndOffset = fold.EndOffset;
                        tag.IsFolding = true;
                    }
                    else
                    {
                        tag.EndOffset = lineStart.EndOffset;
                    }

                    tag.EndLine = editor.Document.GetLineByOffset(tag.EndOffset).LineNumber;

                    foreach (Model.Attribute attr in tag.Attributes)
                    {
                        attr.NameEndOffset = attr.NameStartOffset + attr.Name.Length;
                        attr.ValueEndOffset = attr.ValueStartOffset + attr.Value.Length;

                        //string name = editor.Text.Substring(attr.NameStartOffset, attr.NameEndOffset - attr.NameStartOffset);
                        //string value = editor.Text.Substring(attr.ValueStartOffset, attr.ValueEndOffset - attr.ValueStartOffset);
                    }

                    tag.Length = tag.EndOffset - tag.StartOffset;
                    string text = editor.Text.Substring(tag.StartOffset, tag.Length);
                    int index = -1;
                    index = text.IndexOf("<");
                    if (index != -1)
                    {
                        tag.OpenStartOffset = tag.StartOffset + index;
                        tag.OpenOffset = tag.StartOffset + index + 1;
                    }
                    index = text.IndexOf(">");
                    if (index != -1)
                    {
                        tag.OpenEndOffset = tag.StartOffset + index + 1;
                    }
                    if (tag.IsFolding)
                    {
                        index = text.LastIndexOf("</");
                        if (index != -1)
                        {
                            tag.CloseStartOffset = tag.StartOffset + index;
                            tag.CloseOffset = tag.StartOffset + index + 2;
                        }
                        index = text.LastIndexOf(">");
                        if (index != -1)
                        {
                            tag.CloseEndOffset = tag.StartOffset + index + 1;
                        }
                        tag.IsType = true;
                    }
                    else
                    {
                        bool result = true;

                        // if element is standard format <Tagname></Tagname> 
                        index = text.LastIndexOf("</");
                        if (index != -1)
                        {
                            tag.CloseStartOffset = tag.StartOffset + index;
                            tag.CloseOffset = tag.StartOffset + index + 2;
                            result &= true;
                        }
                        else
                        {
                            result &= false;
                        }

                        index = text.LastIndexOf(">");
                        if (index != -1)
                        {
                            tag.CloseEndOffset = tag.StartOffset + index + 1;
                            result &= true;
                        }
                        else
                        {
                            result &= false;
                        }
                        tag.IsType = true;

                        if (result)
                            continue;

                        // if element is standard format <Tagname />
                        index = text.LastIndexOf("/>");
                        if (index != -1)
                        {
                            tag.CloseStartOffset = tag.StartOffset + index;
                            tag.CloseOffset = tag.StartOffset + index;
                            tag.OpenEndOffset = tag.StartOffset + index;
                        }
                        tag.IsType = false;
                    }
                }
            }

            SortTags(temp);
        }
        void ClearHighlight()
        {
            for (int i = editor.TextArea.TextView.LineTransformers.Count - 1; i >= 0; i--)
            {
                if (editor.TextArea.TextView.LineTransformers[i] is Colorize)
                    editor.TextArea.TextView.LineTransformers.RemoveAt(i);
            }
        }
        void AddHighlight(Tag tag)
        {
            try
            {
                if (tag != null)
                {
                    DocumentLine line1 = editor.Document.GetLineByOffset(tag.OpenOffset);
                    if (line1 != null)
                        editor.TextArea.TextView.LineTransformers.Add(new Colorize(new Tuple<int, int, int>(line1.LineNumber, tag.OpenOffset, tag.OpenOffset + tag.Name.Length)));

                    DocumentLine line2 = editor.Document.GetLineByOffset(tag.CloseOffset);
                    if (line2 != null)
                    {
                        if (tag.IsType)
                            editor.TextArea.TextView.LineTransformers.Add(new Colorize(new Tuple<int, int, int>(line2.LineNumber, tag.CloseOffset, tag.CloseOffset + tag.Name.Length)));
                        else
                            editor.TextArea.TextView.LineTransformers.Add(new Colorize(new Tuple<int, int, int>(line2.LineNumber, tag.CloseOffset, tag.CloseOffset)));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
        }
        void SortTags(List<Tag> lstTags)
        {
            tags.Clear();
            tvTag.Clear();

            lstTags = lstTags.OrderByDescending(x => x.Length).OrderBy(x => x.StartOffset).ToList();

            foreach (Tag tag in lstTags)
            {
                tags.Add(tag);

                if (tag.Level == 0)
                {
                    TreeNode node = new TreeNode(tag.Name) { Tag = tag };
                    tvTag.Add(node);
                    SetLevelTag(node, tag, tag.Level + 1, lstTags);
                }
            }

            lbTag.SelectedIndex = -1;
            tvTag.SelectedNode = null;

            tvTag.ExpandAll();
        }
        void SetLevelTag(TreeNode node, Tag tag, int level, List<Tag> lstTags)
        {
            tag.Level = level;

            foreach (Tag _tag in lstTags)
            {
                if (_tag.ID != tag.ID && _tag.Level == 0 && _tag.StartOffset > tag.StartOffset && _tag.EndOffset < tag.EndOffset)
                {
                    TreeNode _node = new TreeNode(_tag.Name) { Tag = _tag };
                    tvTag.Add(node, _node);
                    SetLevelTag(_node, _tag, tag.Level + 1, lstTags);
                }
            }
        }
    }
}
