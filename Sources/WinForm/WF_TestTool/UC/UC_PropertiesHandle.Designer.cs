﻿namespace WF_TestTool.UC
{
    partial class UC_PropertiesHandle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkDisplayed = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rbDisplayed_Yes = new System.Windows.Forms.RadioButton();
            this.rbDisplayed_No = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbEnabled_Yes = new System.Windows.Forms.RadioButton();
            this.rbEnabled_No = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.numLocation_Y = new System.Windows.Forms.NumericUpDown();
            this.numLocation_X = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.rbSelected_Yes = new System.Windows.Forms.RadioButton();
            this.rbSelected_No = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.numSize_Width = new System.Windows.Forms.NumericUpDown();
            this.numSize_Height = new System.Windows.Forms.NumericUpDown();
            this.chkSize = new System.Windows.Forms.CheckBox();
            this.chkLocation = new System.Windows.Forms.CheckBox();
            this.chkSelected = new System.Windows.Forms.CheckBox();
            this.chkEnabled = new System.Windows.Forms.CheckBox();
            this.lbTitle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numTimeout = new System.Windows.Forms.NumericUpDown();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLocation_Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLocation_X)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSize_Width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSize_Height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.Controls.Add(this.chkDisplayed, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.chkSize, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.chkLocation, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.chkSelected, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.chkEnabled, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.numTimeout, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtTarget, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(450, 250);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chkDisplayed
            // 
            this.chkDisplayed.AutoSize = true;
            this.chkDisplayed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkDisplayed.Location = new System.Drawing.Point(23, 27);
            this.chkDisplayed.Name = "chkDisplayed";
            this.chkDisplayed.Size = new System.Drawing.Size(72, 25);
            this.chkDisplayed.TabIndex = 0;
            this.chkDisplayed.Text = "Displayed";
            this.chkDisplayed.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.rbDisplayed_Yes, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbDisplayed_No, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(101, 27);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(324, 25);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // rbDisplayed_Yes
            // 
            this.rbDisplayed_Yes.AutoSize = true;
            this.rbDisplayed_Yes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbDisplayed_Yes.Location = new System.Drawing.Point(3, 3);
            this.rbDisplayed_Yes.Name = "rbDisplayed_Yes";
            this.rbDisplayed_Yes.Size = new System.Drawing.Size(43, 19);
            this.rbDisplayed_Yes.TabIndex = 0;
            this.rbDisplayed_Yes.Text = "Yes";
            this.rbDisplayed_Yes.UseVisualStyleBackColor = true;
            // 
            // rbDisplayed_No
            // 
            this.rbDisplayed_No.AutoSize = true;
            this.rbDisplayed_No.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbDisplayed_No.Location = new System.Drawing.Point(62, 3);
            this.rbDisplayed_No.Name = "rbDisplayed_No";
            this.rbDisplayed_No.Size = new System.Drawing.Size(39, 19);
            this.rbDisplayed_No.TabIndex = 1;
            this.rbDisplayed_No.Text = "No";
            this.rbDisplayed_No.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.rbEnabled_Yes, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rbEnabled_No, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(101, 58);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(324, 25);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // rbEnabled_Yes
            // 
            this.rbEnabled_Yes.AutoSize = true;
            this.rbEnabled_Yes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbEnabled_Yes.Location = new System.Drawing.Point(3, 3);
            this.rbEnabled_Yes.Name = "rbEnabled_Yes";
            this.rbEnabled_Yes.Size = new System.Drawing.Size(43, 19);
            this.rbEnabled_Yes.TabIndex = 0;
            this.rbEnabled_Yes.Text = "Yes";
            this.rbEnabled_Yes.UseVisualStyleBackColor = true;
            // 
            // rbEnabled_No
            // 
            this.rbEnabled_No.AutoSize = true;
            this.rbEnabled_No.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbEnabled_No.Location = new System.Drawing.Point(62, 3);
            this.rbEnabled_No.Name = "rbEnabled_No";
            this.rbEnabled_No.Size = new System.Drawing.Size(39, 19);
            this.rbEnabled_No.TabIndex = 1;
            this.rbEnabled_No.Text = "No";
            this.rbEnabled_No.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel5, 3);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.numLocation_Y, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.numLocation_X, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(98, 120);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(330, 25);
            this.tableLayoutPanel5.TabIndex = 9;
            // 
            // numLocation_Y
            // 
            this.numLocation_Y.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numLocation_Y.Location = new System.Drawing.Point(168, 3);
            this.numLocation_Y.Name = "numLocation_Y";
            this.numLocation_Y.Size = new System.Drawing.Size(159, 20);
            this.numLocation_Y.TabIndex = 1;
            // 
            // numLocation_X
            // 
            this.numLocation_X.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numLocation_X.Location = new System.Drawing.Point(3, 3);
            this.numLocation_X.Name = "numLocation_X";
            this.numLocation_X.Size = new System.Drawing.Size(159, 20);
            this.numLocation_X.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.rbSelected_Yes, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.rbSelected_No, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(101, 89);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(324, 25);
            this.tableLayoutPanel4.TabIndex = 8;
            // 
            // rbSelected_Yes
            // 
            this.rbSelected_Yes.AutoSize = true;
            this.rbSelected_Yes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbSelected_Yes.Location = new System.Drawing.Point(3, 3);
            this.rbSelected_Yes.Name = "rbSelected_Yes";
            this.rbSelected_Yes.Size = new System.Drawing.Size(43, 19);
            this.rbSelected_Yes.TabIndex = 0;
            this.rbSelected_Yes.Text = "Yes";
            this.rbSelected_Yes.UseVisualStyleBackColor = true;
            // 
            // rbSelected_No
            // 
            this.rbSelected_No.AutoSize = true;
            this.rbSelected_No.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbSelected_No.Location = new System.Drawing.Point(62, 3);
            this.rbSelected_No.Name = "rbSelected_No";
            this.rbSelected_No.Size = new System.Drawing.Size(39, 19);
            this.rbSelected_No.TabIndex = 1;
            this.rbSelected_No.Text = "No";
            this.rbSelected_No.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel6, 3);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.numSize_Width, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.numSize_Height, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(98, 151);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(330, 25);
            this.tableLayoutPanel6.TabIndex = 10;
            // 
            // numSize_Width
            // 
            this.numSize_Width.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numSize_Width.Location = new System.Drawing.Point(3, 3);
            this.numSize_Width.Name = "numSize_Width";
            this.numSize_Width.Size = new System.Drawing.Size(159, 20);
            this.numSize_Width.TabIndex = 0;
            // 
            // numSize_Height
            // 
            this.numSize_Height.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numSize_Height.Location = new System.Drawing.Point(168, 3);
            this.numSize_Height.Name = "numSize_Height";
            this.numSize_Height.Size = new System.Drawing.Size(159, 20);
            this.numSize_Height.TabIndex = 1;
            // 
            // chkSize
            // 
            this.chkSize.AutoSize = true;
            this.chkSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSize.Location = new System.Drawing.Point(23, 151);
            this.chkSize.Name = "chkSize";
            this.chkSize.Size = new System.Drawing.Size(72, 25);
            this.chkSize.TabIndex = 4;
            this.chkSize.Text = "Size";
            this.chkSize.UseVisualStyleBackColor = true;
            // 
            // chkLocation
            // 
            this.chkLocation.AutoSize = true;
            this.chkLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkLocation.Location = new System.Drawing.Point(23, 120);
            this.chkLocation.Name = "chkLocation";
            this.chkLocation.Size = new System.Drawing.Size(72, 25);
            this.chkLocation.TabIndex = 3;
            this.chkLocation.Text = "Location";
            this.chkLocation.UseVisualStyleBackColor = true;
            // 
            // chkSelected
            // 
            this.chkSelected.AutoSize = true;
            this.chkSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSelected.Location = new System.Drawing.Point(23, 89);
            this.chkSelected.Name = "chkSelected";
            this.chkSelected.Size = new System.Drawing.Size(72, 25);
            this.chkSelected.TabIndex = 2;
            this.chkSelected.Text = "Selected";
            this.chkSelected.UseVisualStyleBackColor = true;
            // 
            // chkEnabled
            // 
            this.chkEnabled.AutoSize = true;
            this.chkEnabled.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkEnabled.Location = new System.Drawing.Point(23, 58);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Size = new System.Drawing.Size(72, 25);
            this.chkEnabled.TabIndex = 1;
            this.chkEnabled.Text = "Enabled";
            this.chkEnabled.UseVisualStyleBackColor = true;
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTitle, 6);
            this.lbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTitle.Location = new System.Drawing.Point(3, 0);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(444, 24);
            this.lbTitle.TabIndex = 20;
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(23, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 26);
            this.label3.TabIndex = 18;
            this.label3.Text = "Timeout";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimeout
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.numTimeout, 3);
            this.numTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numTimeout.Location = new System.Drawing.Point(101, 208);
            this.numTimeout.Name = "numTimeout";
            this.numTimeout.Size = new System.Drawing.Size(324, 20);
            this.numTimeout.TabIndex = 19;
            // 
            // txtTarget
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtTarget, 3);
            this.txtTarget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTarget.Location = new System.Drawing.Point(101, 182);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(324, 20);
            this.txtTarget.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(23, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 26);
            this.label1.TabIndex = 12;
            this.label1.Text = "Target";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UC_PropertiesHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UC_PropertiesHandle";
            this.Size = new System.Drawing.Size(450, 250);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLocation_Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLocation_X)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSize_Width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSize_Height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox chkDisplayed;
        private System.Windows.Forms.CheckBox chkEnabled;
        private System.Windows.Forms.CheckBox chkSelected;
        private System.Windows.Forms.CheckBox chkLocation;
        private System.Windows.Forms.CheckBox chkSize;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numTimeout;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.RadioButton rbDisplayed_Yes;
        private System.Windows.Forms.RadioButton rbDisplayed_No;
        private System.Windows.Forms.RadioButton rbEnabled_Yes;
        private System.Windows.Forms.RadioButton rbEnabled_No;
        private System.Windows.Forms.NumericUpDown numLocation_Y;
        private System.Windows.Forms.NumericUpDown numLocation_X;
        private System.Windows.Forms.RadioButton rbSelected_Yes;
        private System.Windows.Forms.RadioButton rbSelected_No;
        private System.Windows.Forms.NumericUpDown numSize_Width;
        private System.Windows.Forms.NumericUpDown numSize_Height;
        private System.Windows.Forms.TextBox txtTarget;
    }
}
