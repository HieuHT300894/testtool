﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_DisplayHandle : UC_Base
    {
        public UC_DisplayHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
        }

        public void GetData(DisplayObject obj)
        {
            obj.sTarget = txtTarget.Text.Trim();
            obj.sValue = txtValue.Text.Trim();
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            DisplayObject obj = new DisplayObject();
            GetData(obj);

            return DisplayHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            txtTarget.Format();
            txtValue.Format();
            numTimeout.Format();

            string sTarget = string.Empty;
            string sValue = string.Empty;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.Target] != null)
                    sTarget = xmlInput.Attributes[Define.Instance.Target].Value;
                if (xmlInput.Attributes[Define.Instance.Value] != null)
                    sValue = xmlInput.Attributes[Define.Instance.Value].Value;
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            txtTarget.Text = sTarget;
            txtValue.Text = sValue;
            numTimeout.Value = iTimeout;
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            DisplayObject obj = new DisplayObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Display);
            attrs.Add(Define.Instance.Target, obj.sTarget);
            attrs.Add(Define.Instance.Value, obj.sValue);
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}
