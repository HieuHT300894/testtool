﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using System.Windows.Media;

namespace WF_TestTool.Model
{
    public class CompletionData : ICompletionData
    {
        public object Content { get { return Text; } }

        public object Description { get { return Text; } }

        public ImageSource Image { get; set; }

        public double Priority { get; set; }

        public string Text { get; set; }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, Text);
        }
    }
    public class XMLTagNameData : ICompletionData
    {
        public object Content { get { return Text; } }

        public object Description { get { return Text; } }

        public ImageSource Image { get; set; }

        public double Priority { get; set; }

        public string Text { get; set; }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            AnchorSegment segment = completionSegment as AnchorSegment;
            textArea.Document.Remove(segment.Offset - 1, 1);
            textArea.Document.Replace(segment.Offset, segment.Length, $"<{Text}></{Text}>");
        }
    }
    public class XMLAttributeNameData : ICompletionData
    {
        public object Content { get { return Text; } }

        public object Description { get { return Text; } }

        public ImageSource Image { get; set; }

        public double Priority { get; set; }

        public string Text { get; set; }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            AnchorSegment segment = completionSegment as AnchorSegment;
            textArea.Document.Replace(segment.Offset, segment.Length, $"{Text}=\"\"");
        }
    }
    public class XmlCompletionData : ICompletionData
    {
        private readonly bool _isAttribute;

        public XmlCompletionData(string text, string description, bool isAttribute)
        {
            _isAttribute = isAttribute;
            Text = text;
            Description = description;
        }

        public ImageSource Image { get { return null; } }

        public string Text { get; private set; }

        public object Content { get { return Text + " (" + Description + ")"; } }

        public object Description { get; private set; }

        public double Priority { get; }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            AnchorSegment segment = completionSegment as AnchorSegment;
            if (_isAttribute)
            {
                textArea.Document.Replace(segment.Offset, segment.Length, $"{Text}=\"\"");
            }
            else
            {
                textArea.Document.Remove(segment.Offset - 1, 1);
                textArea.Document.Replace(segment.Offset, segment.Length, $"<{Text}></{Text}>");
            }
        }
    }
}
