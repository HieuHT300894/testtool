﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WF_TestTool.Model
{

    /// <summary>
    /// All possible events that macro can record
    /// </summary>
    [Serializable]
    public enum MacroEventType
    {
        None,
        Click,
        DoubleClick,
        MouseMove,
        MouseDown,
        MouseUp,
        MouseWheel,
        KeyPress,
        KeyDown,
        KeyUp,
    }

    /// <summary>
    /// Series of events that can be recorded any played back
    /// </summary>
    [Serializable]
    public class MacroEvent
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public MacroEventType MacroEventType { get; set; }
        public EventArgs EventArgs { get; set; }
        public int TimePeriod { get; set; }
        public int TimeLast { get; set; }
    }
}
