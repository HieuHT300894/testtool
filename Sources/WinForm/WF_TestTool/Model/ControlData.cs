﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_TestTool.Model
{
    public class ControlData
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public Control Control { get; set; }
    }
}
