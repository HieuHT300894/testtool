﻿using System;

namespace WF_TestTool.Model
{
    public class Step
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public bool Status { get; set; }
        public int Index { get; set; }
        public Func<string> Func { get; set; }
    }
}
