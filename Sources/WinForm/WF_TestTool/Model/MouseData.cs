﻿using System.Collections.Generic;
using Common;

namespace WF_TestTool.Model
{
    public class MouseData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        static List<MouseData> lstResult;
        public static List<MouseData> GetData()
        {
            if (lstResult == null)
            {
                lstResult = new List<MouseData>();
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.Click), Name = "Click", Value = Define.Instance.Click });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.DblClick), Name = "Double Click", Value = Define.Instance.DblClick });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.MouseDown), Name = "Mouse Down", Value = Define.Instance.MouseDown });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.MouseMove), Name = "Mouse Move", Value = Define.Instance.MouseMove });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.MouseUp), Name = "Mouse Up", Value = Define.Instance.MouseUp });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.MouseOver), Name = "Mouse Over", Value = Define.Instance.MouseOver });
                lstResult.Add(new MouseData() { ID = nameof(Define.Instance.MouseOut), Name = "Mouse Out", Value = Define.Instance.MouseOut });
            }
            return lstResult;
        }
    }
}
