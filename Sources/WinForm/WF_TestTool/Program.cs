﻿using Common;
using SeleniumAPI;
using System;
using System.Windows.Forms;
using WF_TestTool.GUI;

namespace WF_TestTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Extension.InitApp();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());

            Extension.CloseApp();
        }
    }
}
