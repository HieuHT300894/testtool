﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Xml;

namespace Common
{
    public class Define
    {
        static Define _instance;
        public static Define Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Define();
                }
                return _instance;
            }
        }

        #region Tag Name
        //public string Page { get; private set; } = "page";
        public string Step { get; private set; } = "step";
        public string Input { get; private set; } = "input";
        //public string Control { get; private set; } = "control";
        //public string Title { get; private set; } = "title";
        #endregion

        #region Attribute

        #region Common
        public string Type { get; private set; } = "type";
        public string Target { get; private set; } = "target";
        public string Value { get; private set; } = "value";
        public string IsPosition { get; private set; } = "isPosition";
        public string PositionX { get; private set; } = "positionX";
        public string PositionY { get; private set; } = "positionY";
        public string Timeout { get; private set; } = "timeout";
        #endregion

        public string KeyType { get; private set; } = "keyType";

        public string AlertType { get; private set; } = "alertType";
        public string ButtonType { get; private set; } = "buttonType";
        public string AlertText { get; private set; } = "alertText";
        public string AlertInput { get; private set; } = "alertInput";

        public string Path { get; private set; } = "path";

        public string Name { get; private set; } = "name";
        public string Property { get; private set; } = "property";
        public string Displayed { get; private set; } = "displayed";
        public string Enabled { get; private set; } = "enabled";
        public string Selected { get; private set; } = "selected";
        public string Location { get; private set; } = "location";
        public string Size { get; private set; } = "size";
        public string Width { get; private set; } = "width";
        public string Height { get; private set; } = "height";

        public string ID { get; private set; } = "id";
        public string Title { get; private set; } = "title";
        public string Comment { get; private set; } = "comment";

        public string ReadType { get; private set; } = "readType";
        public string Regex { get; private set; } = "regex";
        public string StartLine { get; private set; } = "startLine";
        public string EndLine { get; private set; } = "endLine";
        public string Option { get; private set; } = "option";
        public string Index { get; private set; } = "index";
        #endregion

        #region Default Value
        public int TimeoutValue { get; private set; } = 3;
        public int DelayValue { get; set; } = 100;
        public string dir { get; private set; } = "log";
        public string errorFile { get { return System.IO.Path.Combine(RootPath, dir, $"error-{DateTime.Now.ToString("yyyyMMdd")}.txt"); } }
        public string logFile { get { return System.IO.Path.Combine(RootPath, dir, $"log-{DateTime.Now.ToString("yyyyMMdd")}.txt"); } }
        public enum eStatus : int
        {
            Start = 0,
            Stop = 1,
            Abort = 2
        }
        public string Button { get; private set; } = "button";
        public Color clrNormal { get; private set; } = SystemColors.ControlText;
        public Color clrPass { get; private set; } = Color.Green;
        public Color clrFail { get; private set; } = Color.OrangeRed;
        public Color clrRunning { get; private set; } = Color.Blue;
        public string filterAllFiles { get; private set; } = "All files (*.*)|*.*";
        public string RootPath { get; set; } = Environment.CurrentDirectory;
        public List<XmlNode> Config { get; set; } = new List<XmlNode>();
        public string AppConfig { get { return System.IO.Path.Combine(RootPath, "app.config"); } }

        #region Type Value
        public string Key { get; private set; } = "key";
        public string Alert { get; private set; } = "alert";
        public string Display { get; private set; } = "display";
        public string Text { get; private set; } = "text";
        public string Mouse { get; private set; } = "mouse";
        public string Url { get; private set; } = "url";
        public string Upload { get; private set; } = "upload";
        public string Download { get; private set; } = "download";
        public string Properties { get; private set; } = "properties";
        public string Popup { get; private set; } = "popup";
        public string ReadFile { get; private set; } = "readFile";
        public string Scroll { get; private set; } = "scroll";
        #endregion

        #region Alert
        public enum eAlertType
        {
            None = 0,
            SimpleAlert = 1,
            PromptAlert = 2,
            ConfirmationAlert = 3,
            OK = 4,
            Cancel = 5
        }
        #endregion

        #region Display

        #endregion

        #region Input

        #endregion

        #region Mouse handle
        public string Click { get; private set; } = "click";
        public string MouseDown { get; private set; } = "mousedown";
        public string MouseUp { get; private set; } = "mouseup";
        public string MouseOver { get; private set; } = "mouseover";
        public string MouseMove { get; private set; } = "mousemove";
        public string MouseOut { get; private set; } = "mouseout";
        public string DblClick { get; private set; } = "dblclick";
        #endregion

        #region Read File       
        public enum eReadType : int
        {
            None = 0,
            First = 1,
            Last = 2,
            Custom = 3
        }
        #endregion

        #endregion

        #region  Display Text
        public string Mouse_Text { get; private set; } = "Mouse";
        public string Key_Text { get; private set; } = "Key";
        public string Display_Text { get; private set; } = "Display";
        public string Url_Text { get; private set; } = "Url";
        public string Upload_Text { get; private set; } = "Upload File";
        public string Input_Text { get; private set; } = "Input";
        public string Alert_Text { get; private set; } = "Alert";
        public string SimpleAlert_Text { get; private set; } = "Simple Alert";
        public string PromptAlert_Text { get; private set; } = "Prompt Alert";
        public string ConfirmationAlert_Text { get; private set; } = "Confirmation Alert";
        public string Properties_Text { get; private set; } = "Properties";
        public string Popup_Text { get; private set; } = "Popup";
        public string ReadFile_Text { get; private set; } = "Read File";
        public string Scroll_Text { get; private set; } = "Scroll";
        #endregion

        #region Process
        public Process BrowserProcess { get; set; }
        #endregion
    }
}
