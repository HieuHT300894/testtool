﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Log
    {
        static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Debug(params object[] values)
        {
            string text = string.Join(" ", values.Where(x => x != null).Select(x => x.ToString()).ToArray());
            logger.Debug(text);
        }
        public static void Debug(MethodBase mBase, params object[] values)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("time", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            dic.Add("class", mBase.DeclaringType.Name);
            dic.Add("method", mBase.Name);
            dic.Add("values", values);
            logger.Debug(dic.SerializeObjectToJson());
        }

        public static void Error(MethodBase mBase, Exception ex)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("time", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            dic.Add("class", mBase.DeclaringType.Name);
            dic.Add("method", mBase.Name);
            dic.Add("exception", ex);
            logger.Error(dic.SerializeObjectToJson());
        }
    }
}
